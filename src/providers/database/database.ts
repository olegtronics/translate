import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import{ BehaviorSubject } from 'rxjs/Rx';
import { Storage } from '@ionic/storage';
import { Platform } from 'ionic-angular';

@Injectable()
export class DatabaseProvider {
  database: SQLiteObject;
  private databaseReady: BehaviorSubject<boolean>;
  
  constructor(private platform: Platform, public http: Http, private sqlite: SQLite, private storage: Storage) {
    this.databaseReady = new BehaviorSubject(false);
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      })
      .then((db: SQLiteObject) => {
        this.database = db;
        this.storage.get('database_filled').then(val => {
          if(val) {
            this.databaseReady.next(true);
          }
          else {
            db.executeSql('CREATE TABLE IF NOT EXISTS words(id integer primary key, lfrom text, lto text, lfromtxt text, ltotxt text, ttime text, tdel integer)', {})
            .then(() => this.storage.set('database_filled', true))
            .catch(e => console.log(e));
  
            db.executeSql('CREATE TABLE IF NOT EXISTS setts(id integer primary key, gend text, voicespeed integer, autoplay integer, preflangfrom text, preflangto text, lastlangfrom text, lastlangto text, registered integer, modified integer)', {})
              .then(() => {
                this.storage.set('database_filled', true);
                let thetime = new Date().getTime();
                db.executeSql('INSERT INTO setts (id, gend, voicespeed, autoplay, preflangfrom, preflangto, lastlangfrom, lastlangto, registered, modified) VALUES (?,?,?,?,?,?,?,?,?,?)', [1, 'male', 100, 0, 0, 0, 0, 0, thetime, thetime]).then((res) => {}, (error) => {});
              })
              .catch(e => console.log(e));
          }
        })

      })
      .catch(e => console.log(e));
    });
  }

  stringifi(data) {
    return JSON.stringify(data);
  }

  getDatabaseState() {
    return this.databaseReady.asObservable();
  }

  addDb(lfrom, lto, lfromtxt, ltotxt) {
    let letfrom = this.stringifi(lfrom);
    let letto = this.stringifi(lto);
    let data = [letfrom, letto, lfromtxt, ltotxt, new Date().getTime(), 0];
    this.database.executeSql("INSERT INTO words (lfrom, lto, lfromtxt, ltotxt, ttime, tdel) VALUES (?,?,?,?,?,?)", data).then((res) => {
        // console.log("INSERTED: " + this.stringifi(res));
    }, (error) => {
        console.log("ERROR: " + this.stringifi(error.err));
    });
  }

  getDB() {
    return this.database.executeSql("SELECT * FROM words WHERE tdel != ? ORDER BY id DESC", [1]).then((res) => {
      let words = [];
      if(res.rows.length > 0) {
        for(var i = 0; i < res.rows.length; i++) {
          let reslfrom = JSON.parse(res.rows.item(i).lfrom);
          let reslto = JSON.parse(res.rows.item(i).lto);
          words.push({id: res.rows.item(i).id, lfrom: reslfrom, lto: reslto, lfromtxt: res.rows.item(i).lfromtxt, ltotxt: res.rows.item(i).ltotxt, ttime: res.rows.item(i).ttime});
        }
      }
      return words;
    }, (error) => {
        console.log("ERROR: " + this.stringifi(error));
    });
  }

  getDDB(id) {
    let data = [id, 1];
    return this.database.executeSql("SELECT * FROM words WHERE id=? && tdel != ?", data).then((res) => {
      let word = [];
      if(res.rows.length > 0) {
        for(var i = 0; i < res.rows.length; i++) {
          if(res.rows.item(i).tdel != '1') {
            let reslfrom = JSON.parse(res.rows.item(i).lfrom);
            let reslto = JSON.parse(res.rows.item(i).lto);
            word.push({id: res.rows.item(i).id, lfrom: reslfrom, lto: reslto, ltotxt: res.rows.item(i).ltotxt, ttime: res.rows.item(i).ttime});
          }
        }
      }
      return word;
    }, (error) => {
        console.log("ERROR: " + this.stringifi(error));
    });
  }

  updDDB(id, lfrom, lto, lfromtxt, ltotxt, ttime, tdel) {
    let letfrom = this.stringifi(lfrom);
    let letto = this.stringifi(lto);
    let data = [letfrom, letto, lfromtxt, ltotxt, ttime, tdel, id];
    this.database.executeSql("UPDATE words SET lfrom=?, lto=?, lfromtxt=?, ltotxt=?, ttime=?, tdel=? WHERE id =?", data).then((res) => {
      // console.log("OK: " + this.stringifi(res));
    }, (error) => {
        console.log("ERROR: " + this.stringifi(error));
    });
  }

  delDB() {
    this.database.executeSql("UPDATE words SET tdel=?", [1]).then((res) => {
        // console.log("OK: " + this.stringifi(res));
    }, (error) => {
        console.log("ERROR: " + this.stringifi(error));
    });
  }

  getSetts() {

    return this.database.executeSql("SELECT * FROM setts WHERE id = ?", [1]).then((res) => {
      let setts = [];
      if(res.rows.length > 0) {
        for(var i = 0; i < res.rows.length; i++) {
          setts.push(res.rows.item(i));
        }
      }
      return setts;
    }, (error) => {
        console.log("ERROR: " + this.stringifi(error));
    });

  }

  updSetts(gend, voicespeed, autoplay) {

    let data = [gend, voicespeed, autoplay, 0, 0, 0, 0, new Date().getTime(), 1];
    this.database.executeSql("UPDATE setts SET gend=?, voicespeed=?, autoplay=?, preflangfrom=?, preflangto=?, lastlangfrom=?, lastlangto=?, modified=? WHERE id = ?", data).then((res) => {
      // console.log("OK: " + this.stringifi(res));
    }, (error) => {
        console.log("ERROR: " + this.stringifi(error));
    });

  }

}

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class TranslationProvider {

  data: string;
  errorMsg: string ;
  sourceWord: string;
  translatedLanguageCode: string;
  languages = [];
  returnlang = [];

  constructor(public http: Http) {}

  translateText(sourceText, targetLanguageCode) {
    
    return this.http.get("https://www.googleapis.com/language/translate/v2?key=AIzaSyAFu7HMZUmWfMdYg6LNX5fNL6AUoKdc9KU&target=" + targetLanguageCode + "&q=" + decodeURI(sourceText))
      .map(response => {
        this.data = response.json().data.translations[0];
        return this.data;
      }).catch(this.handleError);

  }

  private handleError(error: any) {
    this.errorMsg = (error.message) ? error.message :
      error.status ? `${error.status}   -  ${error.statusText} ` : 'Server error';
    return Observable.throw(this.errorMsg);
  }

  getLanguages(plat) {

    // 0 - iOS, 1 - Android, 2 - Both

    this.languages = [
      {
        languageCode: 'ar',
        languageName: 'Arabic',
        languageIndex: 'ar-SA',
        languagePlatf: '2'
      },
      // {
      //   languageCode: 'ar',
      //   languageName: 'Arabic (Saudi Arabia)',
      //   languageIndex: 'ar-SA',
      //   languagePlatf: '2'
      // },
      // {
      //   languageCode: 'ar',
      //   languageName: 'Arabic (Iraq)',
      //   languageIndex: 'ar-IQ',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'ar',
      //   languageName: 'Arabic (Jordan)',
      //   languageIndex: 'ar-JO',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'ar',
      //   languageName: 'Arabic (U.A.E.)',
      //   languageIndex: 'ar-AE',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'ar',
      //   languageName: 'Arabic (Bahrain)',
      //   languageIndex: 'ar-BH',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'ar',
      //   languageName: 'Arabic (Algeria)',
      //   languageIndex: 'ar-DZ',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'ar',
      //   languageName: 'Arabic (Kuwait)',
      //   languageIndex: 'ar-KW',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'ar',
      //   languageName: 'Arabic (Morocco)',
      //   languageIndex: 'ar-MA',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'ar',
      //   languageName: 'Arabic (Tunisia)',
      //   languageIndex: 'ar-TN',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'ar',
      //   languageName: 'Arabic (Oman)',
      //   languageIndex: 'ar-OM',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'ar',
      //   languageName: 'Arabic (Qatar)',
      //   languageIndex: 'ar-QA',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'ar',
      //   languageName: 'Arabic (Lebanon)',
      //   languageIndex: 'ar-LB',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'ar',
      //   languageName: 'Arabic (Egypt)',
      //   languageIndex: 'ar-EG',
      //   languagePlatf: '1'
      // },
      {
        languageCode: 'eu',
        languageName: 'Basque',
        languageIndex: 'eu-ES',
        languagePlatf: '1'
      },
      {
        languageCode: 'bg',
        languageName: 'Bulgarian',
        languageIndex: 'bg-BG',
        languagePlatf: '1'
      },
      {
        languageCode: 'ca',
        languageName: 'Catalan',
        languageIndex: 'ca-ES',
        languagePlatf: '2'
      },
      {
        languageCode: 'zh-CN',
        languageName: 'Chinese',
        languageIndex: 'zh-CN',
        languagePlatf: '2'
      },
      // {
      //   languageCode: 'zh-CN',
      //   languageName: 'Chinese (China)',
      //   languageIndex: 'zh-CN',
      //   languagePlatf: '2'
      // },
      // {
      //   languageCode: 'zh-TW',
      //   languageName: 'Chinese (Taiwan)',
      //   languageIndex: 'zh-TW',
      //   languagePlatf: '0'
      // },
      // {
      //   languageCode: 'zh-HK',
      //   languageName: 'Chinese (Hong Kong)',
      //   languageIndex: 'zh-HK',
      //   languagePlatf: '0'
      // },
      {
        languageCode: 'hr',
        languageName: 'Croatian',
        languageIndex: 'hr-HR',
        languagePlatf: '2'
      },
      {
        languageCode: 'cs',
        languageName: 'Czech',
        languageIndex: 'cs-CZ',
        languagePlatf: '2'
      },
      {
        languageCode: 'da',
        languageName: 'Danish',
        languageIndex: 'da-DK',
        languagePlatf: '2'
      },
      {
        languageCode: 'nl',
        languageName: 'Dutch',
        languageIndex: 'nl-NL',
        languagePlatf: '2'
      },
      // {
      //   languageCode: 'nl',
      //   languageName: 'Dutch (Netherlands)',
      //   languageIndex: 'nl-NL',
      //   languagePlatf: '2'
      // },
      // {
      //   languageCode: 'nl',
      //   languageName: 'Dutch (Belgium)',
      //   languageIndex: 'nl-BE',
      //   languagePlatf: '0'
      // },
      {
        languageCode: 'en',
        languageName: 'English',
        languageIndex: 'en-GB',
        languagePlatf: '2'
      },
      // {
      //   languageCode: 'en',
      //   languageName: 'English (Great Britain)',
      //   languageIndex: 'en-GB',
      //   languagePlatf: '2'
      // },
      // {
      //   languageCode: 'en',
      //   languageName: 'English (United States)',
      //   languageIndex: 'en-US',
      //   languagePlatf: '2'
      // },
      // {
      //   languageCode: 'en',
      //   languageName: 'English (Southern Africa)',
      //   languageIndex: 'en-ZA',
      //   languagePlatf: '2'
      // },
      // {
      //   languageCode: 'en',
      //   languageName: 'English (Australia)',
      //   languageIndex: 'en-AU',
      //   languagePlatf: '2'
      // },
      // {
      //   languageCode: 'en',
      //   languageName: 'English (Canada)',
      //   languageIndex: 'en-CA',
      //   languagePlatf: '2'
      // },
      // {
      //   languageCode: 'en',
      //   languageName: 'English (India)',
      //   languageIndex: 'en-IN',
      //   languagePlatf: '2'
      // },
      // {
      //   languageCode: 'en',
      //   languageName: 'English (Phillippines)',
      //   languageIndex: 'en-PH',
      //   languagePlatf: '2'
      // },
      // {
      //   languageCode: 'en',
      //   languageName: 'English (New Zealand)',
      //   languageIndex: 'en-NZ',
      //   languagePlatf: '2'
      // },
      // {
      //   languageCode: 'en',
      //   languageName: 'English (Ireland)',
      //   languageIndex: 'en-IE',
      //   languagePlatf: '2'
      // },
      // {
      //   languageCode: 'es',
      //   languageName: 'Español (Mexico)',
      //   languageIndex: 'es-MX',
      //   languagePlatf: '2'
      // },
      // {
      //   languageCode: 'es',
      //   languageName: 'Español (Chile)',
      //   languageIndex: 'es-CL',
      //   languagePlatf: '2'
      // },
      // {
      //   languageCode: 'es',
      //   languageName: 'Español (Colombia)',
      //   languageIndex: 'es-CO',
      //   languagePlatf: '2'
      // },
      {
        languageCode: 'es',
        languageName: 'Español',
        languageIndex: 'es-US',
        languagePlatf: '2'
      },
      // {
      //   languageCode: 'es',
      //   languageName: 'Español (Bolivia)',
      //   languageIndex: 'es-BO',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'es',
      //   languageName: 'Español (Costa-Rica)',
      //   languageIndex: 'es-CR',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'es',
      //   languageName: 'Español (Argentina)',
      //   languageIndex: 'es-Ar',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'es',
      //   languageName: 'Español (Ecuador)',
      //   languageIndex: 'es-EC',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'es',
      //   languageName: 'Español (El Salvador)',
      //   languageIndex: 'es-SV',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'es',
      //   languageName: 'Español (Guatemala)',
      //   languageIndex: 'es-GT',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'es',
      //   languageName: 'Español (Honduras)',
      //   languageIndex: 'es-HN',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'es',
      //   languageName: 'Español (Nicaragua)',
      //   languageIndex: 'es-NI',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'es',
      //   languageName: 'Español (Panama)',
      //   languageIndex: 'es-PA',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'es',
      //   languageName: 'Español (Paraguay)',
      //   languageIndex: 'es-PY',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'es',
      //   languageName: 'Español (Peru)',
      //   languageIndex: 'es-PE',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'es',
      //   languageName: 'Español (Puerto Rico)',
      //   languageIndex: 'es-PR',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'es',
      //   languageName: 'Español (Dominican Republic)',
      //   languageIndex: 'es-DO',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'es',
      //   languageName: 'Español (Uruguay)',
      //   languageIndex: 'es-UY',
      //   languagePlatf: '1'
      // },
      // {
      //   languageCode: 'es',
      //   languageName: 'Español (Venezuela)',
      //   languageIndex: 'es-VE',
      //   languagePlatf: '1'
      // },
      {
        languageCode: 'fi',
        languageName: 'Finish',
        languageIndex: 'fi-FI',
        languagePlatf: '2'
      },
      {
        languageCode: 'fr',
        languageName: 'French',
        languageIndex: 'fr-FR',
        languagePlatf: '2'
      },
      // {
      //   languageCode: 'fr',
      //   languageName: 'French (Switzerland)',
      //   languageIndex: 'fr-CH',
      //   languagePlatf: '0'
      // },
      // {
      //   languageCode: 'fr',
      //   languageName: 'French (Canada)',
      //   languageIndex: 'fr-CA',
      //   languagePlatf: '0'
      // },
      // {
      //   languageCode: 'fr',
      //   languageName: 'French (Belgium)',
      //   languageIndex: 'fr-BE',
      //   languagePlatf: '0'
      // },
      {
        languageCode: 'de',
        languageName: 'German',
        languageIndex: 'de-DE',
        languagePlatf: '2'
      },
      // {
      //   languageCode: 'de',
      //   languageName: 'German (Austria)',
      //   languageIndex: 'de-AT',
      //   languagePlatf: '0'
      // },
      // {
      //   languageCode: 'de',
      //   languageName: 'German (Austria)',
      //   languageIndex: 'de-AT',
      //   languagePlatf: '0'
      // },
      // {
      //   languageCode: 'de',
      //   languageName: 'German (Switzerland)',
      //   languageIndex: 'de-CH',
      //   languagePlatf: '0'
      // },
      // {
      //   languageCode: 'de',
      //   languageName: 'German',
      //   languageIndex: 'de-DE',
      //   languagePlatf: '2'
      // },
      {
        languageCode: 'hi',
        languageName: 'Hindi',
        languageIndex: 'hi-IN',
        languagePlatf: '1'
      },
      {
        languageCode: 'hu',
        languageName: 'Hungarian',
        languageIndex: 'hu-HU',
        languagePlatf: '2'
      },
      {
        languageCode: 'is',
        languageName: 'Icelandic',
        languageIndex: 'is-IS',
        languagePlatf: '1'
      },
      {
        languageCode: 'id',
        languageName: 'Indonesian',
        languageIndex: 'id-ID',
        languagePlatf: '2'
      },
      {
        languageCode: 'it',
        languageName: 'Italian',
        languageIndex: 'it-IT',
        languagePlatf: '2'
      },
      // {
      //   languageCode: 'it',
      //   languageName: 'Italian (Switzerland)',
      //   languageIndex: 'it-CH',
      //   languagePlatf: '0'
      // },
      {
        languageCode: 'ja',
        languageName: 'Japanese',
        languageIndex: 'ja-JP',
        languagePlatf: '2'
      },
      {
        languageCode: 'ko',
        languageName: 'Korean',
        languageIndex: 'ko-KR',
        languagePlatf: '2'
      },
      // {
      //   languageCode: 'lv',
      //   languageName: 'Latvian',
      //   languageIndex: 'ar-SA',
      //   languagePlatf: '2'
      // },
      {
        languageCode: 'lt',
        languageName: 'Lithuanian',
        languageIndex: 'lt-LT',
        languagePlatf: '1'
      },
      {
        languageCode: 'nb',
        languageName: 'Norwegian',
        languageIndex: 'nb-NO',
        languagePlatf: '2'
      },
      {
        languageCode: 'fa',
        languageName: 'Persian',
        languageIndex: 'fa-IR',
        languagePlatf: '1'
      },
      {
        languageCode: 'pl',
        languageName: 'Polish',
        languageIndex: 'pl-PL',
        languagePlatf: '2'
      },
      {
        languageCode: 'pt',
        languageName: 'Portuguese',
        languageIndex: 'pt-PT',
        languagePlatf: '2'
      },
      // {
      //   languageCode: 'pt',
      //   languageName: 'Portuguese (Brasil)',
      //   languageIndex: 'pt-BR',
      //   languagePlatf: '2'
      // },
      {
        languageCode: 'ro',
        languageName: 'Romanian',
        languageIndex: 'ro-RO',
        languagePlatf: '2'
      },
      {
        languageCode: 'ru',
        languageName: 'Russian',
        languageIndex: 'ru-RU',
        languagePlatf: '2'
      },
      {
        languageCode: 'sr',
        languageName: 'Serbian',
        languageIndex: 'sr-RS',
        languagePlatf: '2'
      },
      {
        languageCode: 'sk',
        languageName: 'Slovak',
        languageIndex: 'sk-SK',
        languagePlatf: '2'
      },
      {
        languageCode: 'sl',
        languageName: 'Slovenian',
        languageIndex: 'sl-SI',
        languagePlatf: '1'
      },
      {
        languageCode: 'sv',
        languageName: 'Swedish',
        languageIndex: 'sv-SE',
        languagePlatf: '2'
      },
      {
        languageCode: 'th',
        languageName: 'Thai',
        languageIndex: 'th-TH',
        languagePlatf: '2'
      },
      {
        languageCode: 'tr',
        languageName: 'Turkish',
        languageIndex: 'tr-TR',
        languagePlatf: '2'
      },
      {
        languageCode: 'uk',
        languageName: 'Ukrainian',
        languageIndex: 'uk-UA',
        languagePlatf: '2'
      },
      // {
      //   languageCode: 'cy',
      //   languageName: 'Welsh',
      //   languageIndex: 'ar-SA',
      //   languagePlatf: '2'
      // },
      {
        languageCode: 'zu',
        languageName: 'Zulu',
        languageIndex: 'zu-ZA',
        languagePlatf: '1'
      },
      {
        languageCode: 'vi',
        languageName: 'Vietnamese',
        languageIndex: 'vi-VN',
        languagePlatf: '2'
      },
      {
        languageCode: 'ca',
        languageName: 'Catalan',
        languageIndex: 'ca-ES',
        languagePlatf: '2'
      },
      {
        languageCode: 'ms',
        languageName: 'Malay',
        languageIndex: 'ms-MY',
        languagePlatf: '2'
      },
      {
        languageCode: 'el',
        languageName: 'Greek',
        languageIndex: 'el-GR',
        languagePlatf: '2'
      },
      {
        languageCode: 'he',
        languageName: 'Hebrew',
        languageIndex: 'he-IL',
        languagePlatf: '2'
      },
      {
        languageCode: 'af',
        languageName: 'Afrikaans',
        languageIndex: 'af-ZA',
        languagePlatf: '1'
      },
      {
        languageCode: 'gl',
        languageName: 'Galician',
        languageIndex: 'gl-ES',
        languagePlatf: '1'
      },
      {
        languageCode: 'gl',
        languageName: 'Galician',
        languageIndex: 'gl-ES',
        languagePlatf: '1'
      }

    ];
    this.returnlang = [];
    for(let i=0;i<this.languages.length;i++) {
      if(this.languages[i].languagePlatf == plat || this.languages[i].languagePlatf == 2) {
        this.returnlang.push(this.languages[i]);
      }
    }

    return this.returnlang;

  }

}

import { Component, ViewChild } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { trigger,state,style,transition,animate } from '@angular/animations';

@Component({
  templateUrl: 'tabs.html',
  styles: [`
  p#x {
    position: absolute;
    z-index: 11;
    bottom:10%;
    left: 50%;
    margin: 0 0 -32px -31px;
    width:62px;
    text-align:center;
    content: url(../../assets/icon/mic_ring_in.svg);
    opacity: 0.9;
  }
  p#y {
    position: absolute;
    z-index: 11;
    bottom:10%;
    left: 50%;
    margin: 0 0 -39px -38px;
    width:76px;
    text-align:center;
    content: url(../../assets/icon/mic_ring_out.svg);
    opacity: 0.9;
  }
  `],
  animations: [
    trigger('flip', [
      state('flipped', style({
          transform: 'rotate(360deg)',
      })),
      transition('* => flipped', animate('4000ms linear'))
    ]),
    trigger('flip2', [
      state('flipped2', style({
          transform: 'rotate(-360deg)',
      })),
      transition('* => flipped2', animate('5000ms linear'))
    ])
  ]
})
export class TabsPage {

  @ViewChild('btmTbs') btmTbs;

  flipState: string = 'flipped';
  flipState2: string = 'flipped2';

  tab1Root = AboutPage;
  tab2Root = HomePage;
  tab3Root = ContactPage;

  constructor() {

  }

  ngAfterViewInit() {
    // console.log(this.btmTbs);
  }
  
  flipStopped(ev) {
    // console.log(ev);
  }

  flipStarted(ev) {
    // console.log(ev);
  }

  toggleFlip() {
    this.flipState = (this.flipState == 'notFlipped') ? 'flipped' : 'notFlipped';
  }

  toggleFlip2() {
    this.flipState2 = (this.flipState2 == 'notFlipped2') ? 'flipped2' : 'notFlipped2';
  }
  
}

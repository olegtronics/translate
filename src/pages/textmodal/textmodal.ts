import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-textmodal',
  templateUrl: 'textmodal.html',
})
export class TextmodalPage {

  word: any;
  fromorto: any;
  langtxt: any;
  title: any;
  langlang: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.word = navParams.get('word');
    this.fromorto = navParams.get('fromorto');
    this.title = navParams.get('title');
    if(this.fromorto == 'from') {
      this.langtxt = this.htmlEnt(this.word.lfromtxt);
      this.langlang = this.word.lfrom.languageName;
    }
    else if(this.fromorto == 'to') {
      this.langtxt = this.htmlEnt(this.word.ltotxt);
      this.langlang = this.word.lto.languageName;
    }
  }

  htmlEnt(value) {
    var element = document.getElementById('htmlentitydecodediv');
    if(value && typeof value === 'string') {
      // strip script/html tags
      value = value.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
      value = value.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
      element.innerHTML = value;
      value = element.textContent;
      element.textContent = '';
    }
    return value;
  }

  close() {
    this.viewCtrl.dismiss();
  }

  save() {
    if(this.fromorto == 'from') {
      this.word.lfromtxt = this.langtxt;
    }
    else if(this.fromorto == 'to') {
      this.word.ltotxt = this.langtxt;
    }
    let data = {word: this.word, fromorto: this.fromorto};
    this.viewCtrl.dismiss(data);
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad TextmodalPage');
  }

}

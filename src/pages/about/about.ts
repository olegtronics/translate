import { Component } from '@angular/core';
import { SpeechRecognition, SpeechRecognitionListeningOptionsAndroid, SpeechRecognitionListeningOptionsIOS } from '@ionic-native/speech-recognition';
import { NavController, Platform, ToastController, AlertController, PopoverController, ModalController } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { SellangPage } from '../sellang/sellang';
import { trigger,state,style,transition,animate } from '@angular/animations';
import { DatabaseProvider } from '../../providers/database/database';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { TextmodalPage } from '../textmodal/textmodal';
import { ActionSheetController } from 'ionic-angular';
import { Clipboard } from '@ionic-native/clipboard';
import { FullscreenPage } from '../fullscreen/fullscreen';
import { SettingsPage } from '../settings/settings';
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
  styles: [`
    p#x {
      position: absolute;
      z-index: 11;
      bottom:10%;
      left: 50%;
      margin: 0 0 3px -31px;
      width:62px;
      text-align:center;
      content: url(assets/icon/mic_ring_in.svg);
      opacity: 1;
    }
    p#y {
      position: absolute;
      z-index: 11;
      bottom:10%;
      left: 50%;
      margin: 0 0 -4px -38px;
      width:76px;
      text-align:center;
      content: url(assets/icon/mic_ring_out.svg);
      opacity: 1;
    }
  `],
  animations: [
    trigger('flip', [
      state('notFlipped', style({
          transform: 'rotate(0deg)',
      })),
      state('flipped', style({
          transform: 'rotate(-360deg)',
      })),
      transition('* => flipped', animate('8000ms linear'))
    ]),
    trigger('flip2', [
      state('notFlipped', style({
          transform: 'rotate(0deg)',
      })),
      state('flipped', style({
          transform: 'rotate(360deg)',
      })),
      transition('* => flipped', animate('6000ms linear'))
    ]),
    trigger('bounce', [
      state('notBounced', style({
          transform: 'scale(1) translateY(0)',
      })),
      state('bounced', style({
          transform: 'scale(1.3) translateY(7px)',
      })),
      transition('* => bounced', animate('200ms linear')),
      transition('* => notBounced', animate('200ms linear'))
    ])
  ]
})
export class AboutPage {

	data;
	errorMsg;
	sourceText: string;
	translatedText: string;
	sourceLanguageCode: string;
	sourceLanguageName = 'Translate a text!';
	targetLanguageCode: string;
  targetLanguageName = 'Choose a language!';

  speechList: Array<string> = [];
  androidOptions: SpeechRecognitionListeningOptionsAndroid;
  iosOptions: SpeechRecognitionListeningOptionsIOS;
  isRecording = false;

  languages = [];
  langin = {
    languageCode: 'en',
    languageName: 'English',
    languageIndex: 'en-GB',
    languagePlatf: '2'
  };
  langout = {
    languageCode: 'en',
    languageName: 'English',
    languageIndex: 'en-GB',
    languagePlatf: '2'
  };

  flipState: string = 'flipped';
  flipState2: string = 'flipped';
  bounceState: string = 'notBounced';
  // words: any = [{lfrom: {languageCode: 'en', languageName: 'English', languageIndex: 'en-GB', languagePlatf: '2'}, lto: {languageCode: 'ru', languageName: 'Russian', languageIndex: 'ru-RU', languagePlatf: '2'}, lfromtxt: 'hello my friend! How are you?', ltotxt: 'Привет! Как дела?', ttime: '1234567890'}];
  words: any = [];
  word: any = [];

  backcol: any = 0;

  constructor(private platform: Platform, private speech: SpeechRecognition, public navCtrl: NavController, public toastCtrl: ToastController, public translation: TranslationProvider, public alertCtrl: AlertController, public popoverCtrl: PopoverController, private databaseProvider: DatabaseProvider, private tts: TextToSpeech, public modalCtrl: ModalController, public actionSheetCtrl: ActionSheetController, private clipboard: Clipboard, private storage: Storage, private network: Network) {
    if(this.platform.is('ios')) {
      this.languages = translation.getLanguages(0);
      // this.languages = ["nl-NL","es-MX","zh-TW","fr-FR","it-IT","vi-VN","en-ZA","ca-ES","es-CL","ko-KR",
      // "ro-RO","fr-CH","en-PH","en-CA","en-SG","en-IN","en-NZ","it-CH","fr-CA","da-DK",
      // "de-AT","pt-BR","yue-CN","zh-CN","sv-SE","es-ES","ar-SA","hu-HU","fr-BE","en-GB",
      // "ja-JP","zh-HK","fi-FI","tr-TR","nb-NO","en-ID","en-SA","pl-PL","id-ID","ms-MY",
      // "el-GR","cs-CZ","hr-HR","en-AE","he-IL","ru-RU","de-CH","en-AU","de-DE","nl-BE",
      // "th-TH","pt-PT","sk-SK","en-US","en-IE","es-CO","uk-UA","es-US"];
    }
    else if(this.platform.is('android')) {
      this.languages = translation.getLanguages(1);
      // this.languages = ["af-ZA", "id-ID", "ms-MY", "ca-ES", "cs-CZ", "da-DK", "de-DE", "en-AU", "en-CA",
      // "en-001", "en-IN", "en-IE", "en-NZ", "en-PH", "en-ZA", "en-GB", "en-US", "es-AR",
      // "es-BO", "es-CL", "es-CO", "es-CR", "es-EC", "es-US", "es-SV", "es-ES", "es-GT",
      // "es-HN", "es-MX", "es-NI", "es-PA", "es-PY", "es-PE", "es-PR", "es-DO", "es-UY",
      // "es-VE", "eu-ES", "fil-PH", "fr-FR", "gl-ES", "hr-HR", "zu-ZA", "is-IS", "it-IT",
      // "lt-LT", "hu-HU", "nl-NL", "nb-NO", "pl-PL", "pt-BR", "pt-PT", "ro-RO", "sl-SI",
      // "sk-SK", "fi-FI", "sv-SE", "vi-VN", "tr-TR", "el-GR", "bg-BG", "ru-RU", "sr-RS",
      // "uk-UA", "he-IL", "ar-IL", "ar-JO", "ar-AE", "ar-BH", "ar-DZ", "ar-SA", "ar-KW",
      // "ar-MA", "ar-TN", "ar-OM", "ar-PS", "ar-QA", "ar-LB", "ar-EG", "fa-IR", "hi-IN",
      // "th-TH", "ko-KR", "cmn-Hans-CN", "cmn-Hans-HK", "cmn-Hant-TW", "yue-Hant-HK",
      // "ja-JP"];
    }
    
    this.platform.ready().then(() => {
      // watch network for a disconnect
      this.network.onDisconnect().subscribe(() => {
        this.presentToast("No internet connection!");
        // alert('network was disconnected :-(');
        // stop disconnect watch
        // disconnectSubscription.unsubscribe();
      });

      this.network.onConnect().subscribe(() => {
        // alert('network connected!');
        setTimeout(() => {
          if (this.network.type === 'wifi') {
            // alert('we got a wifi connection, woohoo!');
            // stop connect watch
            // connectSubscription.unsubscribe();
          }
        }, 3000);
      });

      storage.get('langin').then((val) => {
        if(val) {
          this.langin = JSON.parse(val);
        }
      });
      storage.get('langout').then((val) => {
        if(val) {
          this.langout = JSON.parse(val);
        }
      });
      this.databaseProvider.getDatabaseState().subscribe(rdy => {
        if(rdy) {
          // this.databaseProvider.addDb({languageCode: 'en', languageName: 'English', languageIndex: 'en-GB', languagePlatf: '2'}, {languageCode: 'ru', languageName: 'Russian', languageIndex: 'ru-RU', languagePlatf: '2'}, 'hello my friend! How are you? You&#39;re a stinker!', 'Привет! Как дела?');
          this.loadWordsData();
        }
      })
      
    });

  }

  htmlEnt(value) {
    var element = document.getElementById('htmlentitydecodediv');
    if(value && typeof value === 'string') {
      // strip script/html tags
      value = value.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
      value = value.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
      element.innerHTML = value;
      value = element.textContent;
      element.textContent = '';
    }
    return value;
  }

  goSetts() {
    this.navCtrl.push(SettingsPage);
  }

  removeTxt(word) {
    this.databaseProvider.updDDB(word.id, word.lfrom, word.lto, word.lfromtxt, word.ltotxt, word.ttime, 1);
    setTimeout(()=>{
      this.loadWordsData();
    }, 200);
  }

  fullScreen(word, fromorto) {
    let fullModal = this.modalCtrl.create(FullscreenPage, { word: word, fromorto: fromorto });
    fullModal.onDidDismiss(data => {
      this.backcol = 0;
    });
    fullModal.present();
  }

  presentActionSheet(word, fromorto) {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Copy to clipboard',
          handler: () => {
            this.backcol = 0;
            this.clipboard.copy(word);
          }
        },{
          text: 'Edit phrase',
          handler: () => {
            this.backcol = 1;
            this.textModal(word, fromorto);
          }
        },{
          text: 'Show fullscreen',
          handler: () => {
            this.backcol = 0;
            this.fullScreen(word, fromorto);
          }
        },{
          text: 'Remove',
          role: 'destructive',
          handler: () => {
            this.backcol = 0;
            this.removeTxt(word);
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.backcol = 0;
          }
        }
      ]
    });
    actionSheet.present();
  }

  textModal(word, fromorto) {
    this.backcol = 1;
    let txtModal = this.modalCtrl.create(TextmodalPage, { word: word, fromorto: fromorto, title: 'Edit text'});
    if(word == 0 && fromorto == 0) {
      txtModal = this.modalCtrl.create(TextmodalPage, { word: {lfrom: this.langin, lto: this.langout, lfromtxt: '', ltotxt: '', ttime: '1234567890'}, fromorto: 'from', title: 'Text translation'})
      txtModal.onDidDismiss(result => {
        this.backcol = 0;
        if(result) {
          // console.log(result.word.lfromtxt + ' ' + this.langout.languageCode)
          this.translation.translateText(this.htmlEnt(result.word.lfromtxt), this.langout.languageCode).subscribe(res => {
            
            this.databaseProvider.addDb(result.word.lfrom, result.word.lto, result.word.lfromtxt, res.translatedText);
            setTimeout(()=>{
              this.loadWordsData();
            }, 200);
              
          }, error => {
            this.errorMsg = error;
    
            let alert = this.alertCtrl.create({
              subTitle: 'Error:' + '<br>' + this.errorMsg,
              buttons: ['OK']
            });
            alert.present();
    
          });
        }
      });
    }
    else {
      txtModal.onDidDismiss(result => {
        this.backcol = 0;
        if(result) {
          let fromtxt = result.word.lfromtxt;
          let tolang = result.word.lto.languageCode;
          let from = result.word.lfrom;
          let to = result.word.lto;
          if(fromorto == 'to') {
            fromtxt = result.word.ltotxt;
            tolang = result.word.lfrom.languageCode;
            from = result.word.lto;
            to = result.word.lfrom;
          }
          // console.log('GOT ' + fromorto + ' | ' + fromtxt + ' | ' + tolang + ' | ' + JSON.stringify(word));
          this.translation.translateText(this.htmlEnt(fromtxt), tolang).subscribe(res => {
            // alert('SAVE '+result.word.id+ ' | '+res.translatedText)
            this.databaseProvider.updDDB(result.word.id, from, to, fromtxt, res.translatedText, new Date().getTime(), 0);
            setTimeout(()=>{
              this.loadWordsData();
            }, 200);
              
          }, error => {
            this.errorMsg = error;
    
            let alert = this.alertCtrl.create({
              subTitle: 'Error:' + '<br>' + this.errorMsg,
              buttons: ['OK']
            });
            alert.present();
    
          });
        }
      });
    }
    txtModal.present();
  }

  async listenWords(aword, alang) : Promise<any> {
    try {
      // alert(aword + ' ' + alang)
      await this.databaseProvider.getSetts().then((res) => {
        let speed = parseInt(res[0].voicespeed)/100;
        this.tts.speak({
          text: this.htmlEnt(aword), 
          locale: alang, 
          rate: speed
        })
        .then(() => {})
        .catch((reason: any) => console.log(reason));
      });
    }
    catch(e) {
      console.log(e);
    }
  }

  loadWordsData() {
    this.databaseProvider.getDB().then(res => {
      this.words = res;
      // console.log(JSON.stringify(res))
    })
  }

  ionViewDidLoad() {
    this.flipState = 'flipped';
    this.flipState2 = 'flipped';
    this.loadWordsData();
  }

  toggleBounce() {
    this.bounceState = (this.bounceState == 'notBounced') ? 'bounced' : 'notBounced';
  }

  flipStopped(ev) {
    this.flipState = (this.flipState == 'notFlipped') ? 'flipped' : 'notFlipped';
  }

  flipStopped2(ev) {
    this.flipState2 = (this.flipState2 == 'notFlipped') ? 'flipped' : 'notFlipped';
  }

  flipStarted(ev) {
    
  }

  flipStarted2(ev) {
    
  }

  toggleLangs() {
    let langinold = this.langin;
    let langoutold = this.langout;
    this.langin = langoutold;
    this.langout = langinold;
  }

  openLangin(myEvent) {
    let popdata = {
      type: 1,
      lang: this.langin,
      languages: this.languages
    };
    let popover = this.popoverCtrl.create(SellangPage, popdata);
    popover.onDidDismiss((data) => {
      if(data) {
        let jsonarr = this.databaseProvider.stringifi(data);
        this.storage.set('langin', jsonarr);
        this.langin = data;
      }
    });
    popover.present({
      ev: myEvent
    });
  }

  openLangout(myEvent) {
    let popdata = {
      type: 2,
      lang: this.langout,
      languages: this.languages
    };
    let popover = this.popoverCtrl.create(SellangPage, popdata);
    popover.onDidDismiss((data) => {
      if(data) {
        let jsonarr = this.databaseProvider.stringifi(data);
        this.storage.set('langout', jsonarr);
        this.langout = data;
      }
    });
    popover.present({
      ev: myEvent
    });
  }

	translate(srcTxt) {
    if (this.langout) {

      this.sourceText = srcTxt;
      this.translation.translateText(this.htmlEnt(this.sourceText), this.langout.languageCode).subscribe(data => {

        this.data = data;
        this.translatedText = this.data.translatedText;
        // this.sourceLanguageCode = this.data.detectedSourceLanguage;
        // this.sourceLanguageCodeNameChange();
        // return this.translatedText;
        // alert(this.translatedText)
        
        this.databaseProvider.addDb(this.langin, this.langout, this.sourceText, this.translatedText);
        setTimeout(()=>{
          this.loadWordsData();
        }, 200);
          
      }, error => {
        this.errorMsg = error;

        let alert = this.alertCtrl.create({
          subTitle: 'Error:' + '<br>' + this.errorMsg,
          buttons: ['OK']
        });
        alert.present();

      });

    } else {

      let alert = this.alertCtrl.create({
        subTitle: 'Please choose a target language!',
        buttons: ['OK']
      });
      alert.present();

    }
  };

  presentToast(smth) {
    let toast = this.toastCtrl.create({
      message: smth,
      duration: 3000,
      position: 'middle'
    });
    toast.present();
  }

  isIos() {
    return this.platform.is('ios');
  }

  endlistenForSpeech():void {
    if(this.platform.is('ios')) {
      this.speech.stopListening();
    }
    this.isRecording = false;
    this.toggleBounce();
  }

  listenForSpeech(lang):void {

    this.toggleBounce();

    if(this.isSpeechSupported()) {

      if(this.getPermission(lang)) {

        // this.presentToast(lang.languageIndex);

        this.androidOptions = {
          language: lang.languageIndex,
          matches: 1,
          showPopup: false
        }

        this.iosOptions = {
          language: lang.languageIndex,
          matches: 1
        }

        if(this.platform.is('android')) {
          this.speech.startListening(this.androidOptions).subscribe(data => {
            this.speechList = data;
            // this.presentToast(data[0])
            this.translate(data[0]);
          }, error => this.presentToast(error));
        }
        else if(this.platform.is('ios')) {
          this.speech.startListening(this.iosOptions).subscribe(data => {
            this.speechList = data;
            // this.presentToast(data[0])
            this.translate(data[0]);
          }, error => this.presentToast(error));
        }

        this.isRecording = true;

      }

    }
    else {
      this.presentToast('Your device does not support speech!');
    }

  }

  async getSupportedLanguages():Promise<Array<string>> {
    try{
      const languages = this.speech.getSupportedLanguages();
      // console.log(languages);
      return languages;
    }
    catch(e) {
      console.log(e);
    }
  }

  async hasPermission():Promise<boolean> {
    try{
      const permission = await this.speech.hasPermission();
      // console.log(permission);
      return permission;
    }
    catch(e) {
      console.log(e);
    }
  }

  async getPermission(lang):Promise<void> {
    try{
      const permission = await this.speech.requestPermission();
      if(permission) {
        // this.listenForSpeech(lang);
      }
      // console.log(permission);
      // return permission;
    }
    catch(e) {
      console.log(e);
    }
  }

  async isSpeechSupported():Promise<boolean> {
    const isAvailable = await this.speech.isRecognitionAvailable();
    // console.log(isAvailable);
    return isAvailable;
  }

}

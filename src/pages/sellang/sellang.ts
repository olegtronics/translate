import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-sellang',
  templateUrl: 'sellang.html',
})
export class SellangPage {

  languages = [];
  sellang: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.languages = this.navParams.get('languages');
    this.sellang = this.navParams.get('lang');
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad SellangPage');
  }

  close(lang) {
    this.viewCtrl.dismiss(lang);
  }

}

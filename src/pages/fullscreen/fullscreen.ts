import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { DatabaseProvider } from '../../providers/database/database';

@Component({
  selector: 'page-fullscreen',
  templateUrl: 'fullscreen.html',
})
export class FullscreenPage {

  word: any;
  fromorto: any;
  langtxt: any;
  lang: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private tts: TextToSpeech, private databaseProvider: DatabaseProvider) {
    this.word = navParams.get('word');
    this.fromorto = navParams.get('fromorto');
    if(this.fromorto == 'from') {
      this.langtxt = this.htmlEnt(this.word.lfromtxt);
      this.lang = this.word.lfrom.languageIndex;
    }
    else if(this.fromorto == 'to') {
      this.langtxt = this.htmlEnt(this.word.ltotxt);
      this.lang = this.word.lto.languageIndex;
    }
  }

  htmlEnt(value) {
    var element = document.getElementById('htmlentitydecodediv');
    if(value && typeof value === 'string') {
      // strip script/html tags
      value = value.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
      value = value.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
      element.innerHTML = value;
      value = element.textContent;
      element.textContent = '';
    }
    return value;
  }

  listenWords(aword, alang) {
    this.databaseProvider.getSetts().then((res) => {
      let speed = parseInt(res[0].voicespeed)/10;
      this.tts.speak({text: this.htmlEnt(aword), locale: alang, rate: speed})
      .then(() => {})
      .catch((reason: any) => alert(reason));
    });
  }

  close() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad FullscreenPage');
  }

}

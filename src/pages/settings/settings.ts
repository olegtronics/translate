import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  gender: any = 0;
  speed: any = 10;
  autoplay: any = 0;
  icloud: any = 0;
  something: any = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, private databaseProvider: DatabaseProvider, public alertCtrl: AlertController, private iab: InAppBrowser) {
    this.getDB();
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad SettingsPage');
  }

  getDB() {
    this.databaseProvider.getSetts().then(res => {
      this.gender = res[0].gend;
      this.speed = res[0].voicespeed;
      this.autoplay = res[0].autoplay;
      // console.log(res[0].gend + ' ' + res[0].voicespeed + ' ' + res[0].autoplay)
    })
  }

  emptyDB() {

    let confirm = this.alertCtrl.create({
      title: 'Delete history?',
      message: 'Confirmation will delete all the translation history.',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            // console.log('Disagree clicked');
          }
        },
        {
          text: 'Delete',
          handler: () => {
            // console.log('Agree clicked');
            this.databaseProvider.delDB();
          }
        }
      ]
    });
    confirm.present();

  }

  updDB() {
    // console.log(this.gender + ' ' + this.speed + ' ' + this.autoplay)
    this.databaseProvider.updSetts(this.gender, this.speed, this.autoplay);
  }

  openURL(url) {
    this.iab.create(url, '_system', 'location=yes');
  }

}

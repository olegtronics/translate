webpackJsonp([0],{

/***/ 124:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_speech_recognition__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_translation_translation__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__sellang_sellang__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_animations__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_database_database__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_text_to_speech__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__textmodal_textmodal__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_clipboard__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__fullscreen_fullscreen__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__settings_settings__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_storage__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_network__ = __webpack_require__(267);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};















var AboutPage = (function () {
    function AboutPage(platform, speech, navCtrl, toastCtrl, translation, alertCtrl, popoverCtrl, databaseProvider, tts, modalCtrl, actionSheetCtrl, clipboard, storage, network) {
        var _this = this;
        this.platform = platform;
        this.speech = speech;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.translation = translation;
        this.alertCtrl = alertCtrl;
        this.popoverCtrl = popoverCtrl;
        this.databaseProvider = databaseProvider;
        this.tts = tts;
        this.modalCtrl = modalCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.clipboard = clipboard;
        this.storage = storage;
        this.network = network;
        this.sourceLanguageName = 'Translate a text!';
        this.targetLanguageName = 'Choose a language!';
        this.speechList = [];
        this.isRecording = false;
        this.languages = [];
        this.langin = {
            languageCode: 'en',
            languageName: 'English',
            languageIndex: 'en-GB',
            languagePlatf: '2'
        };
        this.langout = {
            languageCode: 'en',
            languageName: 'English',
            languageIndex: 'en-GB',
            languagePlatf: '2'
        };
        this.flipState = 'flipped';
        this.flipState2 = 'flipped';
        this.bounceState = 'notBounced';
        // words: any = [{lfrom: {languageCode: 'en', languageName: 'English', languageIndex: 'en-GB', languagePlatf: '2'}, lto: {languageCode: 'ru', languageName: 'Russian', languageIndex: 'ru-RU', languagePlatf: '2'}, lfromtxt: 'hello my friend! How are you?', ltotxt: 'Привет! Как дела?', ttime: '1234567890'}];
        this.words = [];
        this.word = [];
        this.backcol = 0;
        if (this.platform.is('ios')) {
            this.languages = translation.getLanguages(0);
            // this.languages = ["nl-NL","es-MX","zh-TW","fr-FR","it-IT","vi-VN","en-ZA","ca-ES","es-CL","ko-KR",
            // "ro-RO","fr-CH","en-PH","en-CA","en-SG","en-IN","en-NZ","it-CH","fr-CA","da-DK",
            // "de-AT","pt-BR","yue-CN","zh-CN","sv-SE","es-ES","ar-SA","hu-HU","fr-BE","en-GB",
            // "ja-JP","zh-HK","fi-FI","tr-TR","nb-NO","en-ID","en-SA","pl-PL","id-ID","ms-MY",
            // "el-GR","cs-CZ","hr-HR","en-AE","he-IL","ru-RU","de-CH","en-AU","de-DE","nl-BE",
            // "th-TH","pt-PT","sk-SK","en-US","en-IE","es-CO","uk-UA","es-US"];
        }
        else if (this.platform.is('android')) {
            this.languages = translation.getLanguages(1);
            // this.languages = ["af-ZA", "id-ID", "ms-MY", "ca-ES", "cs-CZ", "da-DK", "de-DE", "en-AU", "en-CA",
            // "en-001", "en-IN", "en-IE", "en-NZ", "en-PH", "en-ZA", "en-GB", "en-US", "es-AR",
            // "es-BO", "es-CL", "es-CO", "es-CR", "es-EC", "es-US", "es-SV", "es-ES", "es-GT",
            // "es-HN", "es-MX", "es-NI", "es-PA", "es-PY", "es-PE", "es-PR", "es-DO", "es-UY",
            // "es-VE", "eu-ES", "fil-PH", "fr-FR", "gl-ES", "hr-HR", "zu-ZA", "is-IS", "it-IT",
            // "lt-LT", "hu-HU", "nl-NL", "nb-NO", "pl-PL", "pt-BR", "pt-PT", "ro-RO", "sl-SI",
            // "sk-SK", "fi-FI", "sv-SE", "vi-VN", "tr-TR", "el-GR", "bg-BG", "ru-RU", "sr-RS",
            // "uk-UA", "he-IL", "ar-IL", "ar-JO", "ar-AE", "ar-BH", "ar-DZ", "ar-SA", "ar-KW",
            // "ar-MA", "ar-TN", "ar-OM", "ar-PS", "ar-QA", "ar-LB", "ar-EG", "fa-IR", "hi-IN",
            // "th-TH", "ko-KR", "cmn-Hans-CN", "cmn-Hans-HK", "cmn-Hant-TW", "yue-Hant-HK",
            // "ja-JP"];
        }
        this.platform.ready().then(function () {
            // watch network for a disconnect
            _this.network.onDisconnect().subscribe(function () {
                _this.presentToast("No internet connection!");
                // alert('network was disconnected :-(');
                // stop disconnect watch
                // disconnectSubscription.unsubscribe();
            });
            _this.network.onConnect().subscribe(function () {
                // alert('network connected!');
                setTimeout(function () {
                    if (_this.network.type === 'wifi') {
                        // alert('we got a wifi connection, woohoo!');
                        // stop connect watch
                        // connectSubscription.unsubscribe();
                    }
                }, 3000);
            });
            storage.get('langin').then(function (val) {
                if (val) {
                    _this.langin = JSON.parse(val);
                }
            });
            storage.get('langout').then(function (val) {
                if (val) {
                    _this.langout = JSON.parse(val);
                }
            });
            _this.databaseProvider.getDatabaseState().subscribe(function (rdy) {
                if (rdy) {
                    // this.databaseProvider.addDb({languageCode: 'en', languageName: 'English', languageIndex: 'en-GB', languagePlatf: '2'}, {languageCode: 'ru', languageName: 'Russian', languageIndex: 'ru-RU', languagePlatf: '2'}, 'hello my friend! How are you? You&#39;re a stinker!', 'Привет! Как дела?');
                    _this.loadWordsData();
                }
            });
        });
    }
    AboutPage.prototype.htmlEnt = function (value) {
        var element = document.getElementById('htmlentitydecodediv');
        if (value && typeof value === 'string') {
            // strip script/html tags
            value = value.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
            value = value.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
            element.innerHTML = value;
            value = element.textContent;
            element.textContent = '';
        }
        return value;
    };
    AboutPage.prototype.goSetts = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__settings_settings__["a" /* SettingsPage */]);
    };
    AboutPage.prototype.removeTxt = function (word) {
        var _this = this;
        this.databaseProvider.updDDB(word.id, word.lfrom, word.lto, word.lfromtxt, word.ltotxt, word.ttime, 1);
        setTimeout(function () {
            _this.loadWordsData();
        }, 200);
    };
    AboutPage.prototype.fullScreen = function (word, fromorto) {
        var _this = this;
        var fullModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__fullscreen_fullscreen__["a" /* FullscreenPage */], { word: word, fromorto: fromorto });
        fullModal.onDidDismiss(function (data) {
            _this.backcol = 0;
        });
        fullModal.present();
    };
    AboutPage.prototype.presentActionSheet = function (word, fromorto) {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            buttons: [
                {
                    text: 'Copy to clipboard',
                    handler: function () {
                        _this.backcol = 0;
                        _this.clipboard.copy(word);
                    }
                }, {
                    text: 'Edit phrase',
                    handler: function () {
                        _this.backcol = 1;
                        _this.textModal(word, fromorto);
                    }
                }, {
                    text: 'Show fullscreen',
                    handler: function () {
                        _this.backcol = 0;
                        _this.fullScreen(word, fromorto);
                    }
                }, {
                    text: 'Remove',
                    role: 'destructive',
                    handler: function () {
                        _this.backcol = 0;
                        _this.removeTxt(word);
                    }
                }, {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        _this.backcol = 0;
                    }
                }
            ]
        });
        actionSheet.present();
    };
    AboutPage.prototype.textModal = function (word, fromorto) {
        var _this = this;
        this.backcol = 1;
        var txtModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_8__textmodal_textmodal__["a" /* TextmodalPage */], { word: word, fromorto: fromorto, title: 'Edit text' });
        if (word == 0 && fromorto == 0) {
            txtModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_8__textmodal_textmodal__["a" /* TextmodalPage */], { word: { lfrom: this.langin, lto: this.langout, lfromtxt: '', ltotxt: '', ttime: '1234567890' }, fromorto: 'from', title: 'Text translation' });
            txtModal.onDidDismiss(function (result) {
                _this.backcol = 0;
                if (result) {
                    // console.log(result.word.lfromtxt + ' ' + this.langout.languageCode)
                    _this.translation.translateText(_this.htmlEnt(result.word.lfromtxt), _this.langout.languageCode).subscribe(function (res) {
                        _this.databaseProvider.addDb(result.word.lfrom, result.word.lto, result.word.lfromtxt, res.translatedText);
                        setTimeout(function () {
                            _this.loadWordsData();
                        }, 200);
                    }, function (error) {
                        _this.errorMsg = error;
                        var alert = _this.alertCtrl.create({
                            subTitle: 'Error:' + '<br>' + _this.errorMsg,
                            buttons: ['OK']
                        });
                        alert.present();
                    });
                }
            });
        }
        else {
            txtModal.onDidDismiss(function (result) {
                _this.backcol = 0;
                if (result) {
                    var fromtxt_1 = result.word.lfromtxt;
                    var tolang = result.word.lto.languageCode;
                    var from_1 = result.word.lfrom;
                    var to_1 = result.word.lto;
                    if (fromorto == 'to') {
                        fromtxt_1 = result.word.ltotxt;
                        tolang = result.word.lfrom.languageCode;
                        from_1 = result.word.lto;
                        to_1 = result.word.lfrom;
                    }
                    // console.log('GOT ' + fromorto + ' | ' + fromtxt + ' | ' + tolang + ' | ' + JSON.stringify(word));
                    _this.translation.translateText(_this.htmlEnt(fromtxt_1), tolang).subscribe(function (res) {
                        // alert('SAVE '+result.word.id+ ' | '+res.translatedText)
                        _this.databaseProvider.updDDB(result.word.id, from_1, to_1, fromtxt_1, res.translatedText, new Date().getTime(), 0);
                        setTimeout(function () {
                            _this.loadWordsData();
                        }, 200);
                    }, function (error) {
                        _this.errorMsg = error;
                        var alert = _this.alertCtrl.create({
                            subTitle: 'Error:' + '<br>' + _this.errorMsg,
                            buttons: ['OK']
                        });
                        alert.present();
                    });
                }
            });
        }
        txtModal.present();
    };
    AboutPage.prototype.listenWords = function (aword, alang) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        // alert(aword + ' ' + alang)
                        return [4 /*yield*/, this.databaseProvider.getSetts().then(function (res) {
                                var speed = parseInt(res[0].voicespeed) / 100;
                                _this.tts.speak({
                                    text: _this.htmlEnt(aword),
                                    locale: alang,
                                    rate: speed
                                })
                                    .then(function () { })
                                    .catch(function (reason) { return console.log(reason); });
                            })];
                    case 1:
                        // alert(aword + ' ' + alang)
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.log(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AboutPage.prototype.loadWordsData = function () {
        var _this = this;
        this.databaseProvider.getDB().then(function (res) {
            _this.words = res;
            // console.log(JSON.stringify(res))
        });
    };
    AboutPage.prototype.ionViewDidLoad = function () {
        this.flipState = 'flipped';
        this.flipState2 = 'flipped';
        this.loadWordsData();
    };
    AboutPage.prototype.toggleBounce = function () {
        this.bounceState = (this.bounceState == 'notBounced') ? 'bounced' : 'notBounced';
    };
    AboutPage.prototype.flipStopped = function (ev) {
        this.flipState = (this.flipState == 'notFlipped') ? 'flipped' : 'notFlipped';
    };
    AboutPage.prototype.flipStopped2 = function (ev) {
        this.flipState2 = (this.flipState2 == 'notFlipped') ? 'flipped' : 'notFlipped';
    };
    AboutPage.prototype.flipStarted = function (ev) {
    };
    AboutPage.prototype.flipStarted2 = function (ev) {
    };
    AboutPage.prototype.toggleLangs = function () {
        var langinold = this.langin;
        var langoutold = this.langout;
        this.langin = langoutold;
        this.langout = langinold;
    };
    AboutPage.prototype.openLangin = function (myEvent) {
        var _this = this;
        var popdata = {
            type: 1,
            lang: this.langin,
            languages: this.languages
        };
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_4__sellang_sellang__["a" /* SellangPage */], popdata);
        popover.onDidDismiss(function (data) {
            if (data) {
                var jsonarr = _this.databaseProvider.stringifi(data);
                _this.storage.set('langin', jsonarr);
                _this.langin = data;
            }
        });
        popover.present({
            ev: myEvent
        });
    };
    AboutPage.prototype.openLangout = function (myEvent) {
        var _this = this;
        var popdata = {
            type: 2,
            lang: this.langout,
            languages: this.languages
        };
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_4__sellang_sellang__["a" /* SellangPage */], popdata);
        popover.onDidDismiss(function (data) {
            if (data) {
                var jsonarr = _this.databaseProvider.stringifi(data);
                _this.storage.set('langout', jsonarr);
                _this.langout = data;
            }
        });
        popover.present({
            ev: myEvent
        });
    };
    AboutPage.prototype.translate = function (srcTxt) {
        var _this = this;
        if (this.langout) {
            this.sourceText = srcTxt;
            this.translation.translateText(this.htmlEnt(this.sourceText), this.langout.languageCode).subscribe(function (data) {
                _this.data = data;
                _this.translatedText = _this.data.translatedText;
                // this.sourceLanguageCode = this.data.detectedSourceLanguage;
                // this.sourceLanguageCodeNameChange();
                // return this.translatedText;
                // alert(this.translatedText)
                _this.databaseProvider.addDb(_this.langin, _this.langout, _this.sourceText, _this.translatedText);
                setTimeout(function () {
                    _this.loadWordsData();
                }, 200);
            }, function (error) {
                _this.errorMsg = error;
                var alert = _this.alertCtrl.create({
                    subTitle: 'Error:' + '<br>' + _this.errorMsg,
                    buttons: ['OK']
                });
                alert.present();
            });
        }
        else {
            var alert_1 = this.alertCtrl.create({
                subTitle: 'Please choose a target language!',
                buttons: ['OK']
            });
            alert_1.present();
        }
    };
    ;
    AboutPage.prototype.presentToast = function (smth) {
        var toast = this.toastCtrl.create({
            message: smth,
            duration: 3000,
            position: 'middle'
        });
        toast.present();
    };
    AboutPage.prototype.isIos = function () {
        return this.platform.is('ios');
    };
    AboutPage.prototype.endlistenForSpeech = function () {
        if (this.platform.is('ios')) {
            this.speech.stopListening();
        }
        this.isRecording = false;
        this.toggleBounce();
    };
    AboutPage.prototype.listenForSpeech = function (lang) {
        var _this = this;
        this.toggleBounce();
        if (this.isSpeechSupported()) {
            if (this.getPermission(lang)) {
                // this.presentToast(lang.languageIndex);
                this.androidOptions = {
                    language: lang.languageIndex,
                    matches: 1,
                    showPopup: false
                };
                this.iosOptions = {
                    language: lang.languageIndex,
                    matches: 1
                };
                if (this.platform.is('android')) {
                    this.speech.startListening(this.androidOptions).subscribe(function (data) {
                        _this.speechList = data;
                        // this.presentToast(data[0])
                        _this.translate(data[0]);
                    }, function (error) { return _this.presentToast(error); });
                }
                else if (this.platform.is('ios')) {
                    this.speech.startListening(this.iosOptions).subscribe(function (data) {
                        _this.speechList = data;
                        // this.presentToast(data[0])
                        _this.translate(data[0]);
                    }, function (error) { return _this.presentToast(error); });
                }
                this.isRecording = true;
            }
        }
        else {
            this.presentToast('Your device does not support speech!');
        }
    };
    AboutPage.prototype.getSupportedLanguages = function () {
        return __awaiter(this, void 0, void 0, function () {
            var languages;
            return __generator(this, function (_a) {
                try {
                    languages = this.speech.getSupportedLanguages();
                    // console.log(languages);
                    return [2 /*return*/, languages];
                }
                catch (e) {
                    console.log(e);
                }
                return [2 /*return*/];
            });
        });
    };
    AboutPage.prototype.hasPermission = function () {
        return __awaiter(this, void 0, void 0, function () {
            var permission, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.speech.hasPermission()];
                    case 1:
                        permission = _a.sent();
                        // console.log(permission);
                        return [2 /*return*/, permission];
                    case 2:
                        e_2 = _a.sent();
                        console.log(e_2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AboutPage.prototype.getPermission = function (lang) {
        return __awaiter(this, void 0, void 0, function () {
            var permission, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.speech.requestPermission()];
                    case 1:
                        permission = _a.sent();
                        if (permission) {
                            // this.listenForSpeech(lang);
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_3 = _a.sent();
                        console.log(e_3);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AboutPage.prototype.isSpeechSupported = function () {
        return __awaiter(this, void 0, void 0, function () {
            var isAvailable;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.speech.isRecognitionAvailable()];
                    case 1:
                        isAvailable = _a.sent();
                        // console.log(isAvailable);
                        return [2 /*return*/, isAvailable];
                }
            });
        });
    };
    return AboutPage;
}());
AboutPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-about',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/Translate/src/pages/about/about.html"*/'<ion-header no-border [ngClass]="{\'blur\': backcol == 1}">\n  <!-- <ion-navbar class="glob_back_grad" [color]="isRecording ? \'danger\' : \'transparent\'"> -->\n  <ion-navbar transparent>\n    <ion-buttons left>\n      <button ion-button icon-end small outline round color="light" full="true" (click)="openLangin($event)">\n        <small>from: </small>\n        <b *ngIf="langin">{{langin.languageName}}</b>\n        <ion-icon name="arrow-dropdown"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title (click)="toggleLangs()"><img src="assets/icon/switch_language.png" ></ion-title>\n    <ion-buttons right>\n      <button ion-button icon-end small outline round color="light" full="true" (click)="openLangout($event)">\n        <small>to: </small>\n        <b *ngIf="langout">{{langout.languageName}}</b>\n        <ion-icon name="arrow-dropdown"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding [ngClass]="{\'glob_back_grad_blur padbot\': backcol == 1, \'glob_back_grad padbot\': backcol == 0}">\n\n  <ion-card class="transcard" *ngFor="let word of words">\n    <ion-list>\n      <ion-item class="lfrom" text-wrap>\n        <p (click)="presentActionSheet(word, \'from\')">{{word.lfrom.languageName}}</p>\n        <h2 (click)="presentActionSheet(word, \'from\')">{{htmlEnt(word.lfromtxt)}}</h2>\n        <ion-icon (click)="listenWords(word.lfromtxt, word.lfrom.languageIndex)" name="custom_sound_dis" item-end></ion-icon>\n        <ion-fab bottom left edge>\n          <button ion-fab mini class="arrdis"><ion-icon name="custom_arr_blue"></ion-icon></button>\n        </ion-fab>\n      </ion-item>\n      <ion-item class="lto" text-wrap>\n        <p (click)="presentActionSheet(word, \'to\')">{{word.lto.languageName}}</p>\n        <h2 (click)="presentActionSheet(word, \'to\')">{{htmlEnt(word.ltotxt)}}</h2>\n        <ion-icon (click)="listenWords(word.ltotxt, word.lto.languageIndex)" name="custom_sound_dis" item-end></ion-icon>\n      </ion-item>\n      <ion-item *ngIf="isTime">\n        <ion-datetime displayFormat="h:mm A" pickerFormat="h mm A">{{word.ttime}}</ion-datetime>\n      </ion-item>\n    </ion-list>\n  </ion-card>\n\n</ion-content>\n\n<ion-footer no-border [ngClass]="{\'blur\': backcol == 1}">\n  <ion-grid>\n      <ion-row align-items-center>\n          <ion-col col-4 align-self-center text-center (click)="goSetts()">\n            <button ion-button icon-only full [clear]="true">\n              <ion-icon name="custom_settings"></ion-icon>\n            </button>\n          </ion-col>\n          <ion-col col-4 align-self-center text-center (touchstart)="listenForSpeech(langin)" (touchend)="endlistenForSpeech()">\n            <button ion-button icon-only full [clear]="true">\n              <ion-icon name="custom_mic"></ion-icon>\n            </button>\n            <div id="xy" [@bounce]=\'bounceState\'>\n              <p id="x" [@flip]=\'flipState\' (@flip.start)=\'flipStarted($event)\' (@flip.done)=\'flipStopped($event)\'></p>\n              <p id="y" [@flip2]=\'flipState2\' (@flip2.start)=\'flipStarted2($event)\' (@flip2.done)=\'flipStopped2($event)\'></p>\n            </div>\n          </ion-col>\n          <ion-col col-4 align-self-center text-center (click)="textModal(0, 0)">\n            <button ion-button icon-only full [clear]="true">\n              <ion-icon name="custom_translate"></ion-icon>\n            </button>\n          </ion-col>\n      </ion-row>\n  </ion-grid>\n</ion-footer>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/Translate/src/pages/about/about.html"*/,
        styles: ["\n    p#x {\n      position: absolute;\n      z-index: 11;\n      bottom:10%;\n      left: 50%;\n      margin: 0 0 3px -31px;\n      width:62px;\n      text-align:center;\n      content: url(assets/icon/mic_ring_in.svg);\n      opacity: 1;\n    }\n    p#y {\n      position: absolute;\n      z-index: 11;\n      bottom:10%;\n      left: 50%;\n      margin: 0 0 -4px -38px;\n      width:76px;\n      text-align:center;\n      content: url(assets/icon/mic_ring_out.svg);\n      opacity: 1;\n    }\n  "],
        animations: [
            Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["h" /* trigger */])('flip', [
                Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["e" /* state */])('notFlipped', Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["f" /* style */])({
                    transform: 'rotate(0deg)',
                })),
                Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["e" /* state */])('flipped', Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["f" /* style */])({
                    transform: 'rotate(-360deg)',
                })),
                Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["g" /* transition */])('* => flipped', Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["c" /* animate */])('8000ms linear'))
            ]),
            Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["h" /* trigger */])('flip2', [
                Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["e" /* state */])('notFlipped', Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["f" /* style */])({
                    transform: 'rotate(0deg)',
                })),
                Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["e" /* state */])('flipped', Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["f" /* style */])({
                    transform: 'rotate(360deg)',
                })),
                Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["g" /* transition */])('* => flipped', Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["c" /* animate */])('6000ms linear'))
            ]),
            Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["h" /* trigger */])('bounce', [
                Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["e" /* state */])('notBounced', Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["f" /* style */])({
                    transform: 'scale(1) translateY(0)',
                })),
                Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["e" /* state */])('bounced', Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["f" /* style */])({
                    transform: 'scale(1.3) translateY(7px)',
                })),
                Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["g" /* transition */])('* => bounced', Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["c" /* animate */])('200ms linear')),
                Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["g" /* transition */])('* => notBounced', Object(__WEBPACK_IMPORTED_MODULE_5__angular_animations__["c" /* animate */])('200ms linear'))
            ])
        ]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_1__ionic_native_speech_recognition__["a" /* SpeechRecognition */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3__providers_translation_translation__["a" /* TranslationProvider */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_6__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_text_to_speech__["a" /* TextToSpeech */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* ModalController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_clipboard__["a" /* Clipboard */], __WEBPACK_IMPORTED_MODULE_12__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_13__ionic_native_network__["a" /* Network */]])
], AboutPage);

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 146:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 146;

/***/ }),

/***/ 189:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 189;

/***/ }),

/***/ 234:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TranslationProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_throw__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_throw___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_throw__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TranslationProvider = (function () {
    function TranslationProvider(http) {
        this.http = http;
        this.languages = [];
        this.returnlang = [];
    }
    TranslationProvider.prototype.translateText = function (sourceText, targetLanguageCode) {
        var _this = this;
        return this.http.get("https://www.googleapis.com/language/translate/v2?key=AIzaSyAFu7HMZUmWfMdYg6LNX5fNL6AUoKdc9KU&target=" + targetLanguageCode + "&q=" + decodeURI(sourceText))
            .map(function (response) {
            _this.data = response.json().data.translations[0];
            return _this.data;
        }).catch(this.handleError);
    };
    TranslationProvider.prototype.handleError = function (error) {
        this.errorMsg = (error.message) ? error.message :
            error.status ? error.status + "   -  " + error.statusText + " " : 'Server error';
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(this.errorMsg);
    };
    TranslationProvider.prototype.getLanguages = function (plat) {
        // 0 - iOS, 1 - Android, 2 - Both
        this.languages = [
            {
                languageCode: 'ar',
                languageName: 'Arabic',
                languageIndex: 'ar-SA',
                languagePlatf: '2'
            },
            // {
            //   languageCode: 'ar',
            //   languageName: 'Arabic (Saudi Arabia)',
            //   languageIndex: 'ar-SA',
            //   languagePlatf: '2'
            // },
            // {
            //   languageCode: 'ar',
            //   languageName: 'Arabic (Iraq)',
            //   languageIndex: 'ar-IQ',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'ar',
            //   languageName: 'Arabic (Jordan)',
            //   languageIndex: 'ar-JO',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'ar',
            //   languageName: 'Arabic (U.A.E.)',
            //   languageIndex: 'ar-AE',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'ar',
            //   languageName: 'Arabic (Bahrain)',
            //   languageIndex: 'ar-BH',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'ar',
            //   languageName: 'Arabic (Algeria)',
            //   languageIndex: 'ar-DZ',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'ar',
            //   languageName: 'Arabic (Kuwait)',
            //   languageIndex: 'ar-KW',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'ar',
            //   languageName: 'Arabic (Morocco)',
            //   languageIndex: 'ar-MA',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'ar',
            //   languageName: 'Arabic (Tunisia)',
            //   languageIndex: 'ar-TN',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'ar',
            //   languageName: 'Arabic (Oman)',
            //   languageIndex: 'ar-OM',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'ar',
            //   languageName: 'Arabic (Qatar)',
            //   languageIndex: 'ar-QA',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'ar',
            //   languageName: 'Arabic (Lebanon)',
            //   languageIndex: 'ar-LB',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'ar',
            //   languageName: 'Arabic (Egypt)',
            //   languageIndex: 'ar-EG',
            //   languagePlatf: '1'
            // },
            {
                languageCode: 'eu',
                languageName: 'Basque',
                languageIndex: 'eu-ES',
                languagePlatf: '1'
            },
            {
                languageCode: 'bg',
                languageName: 'Bulgarian',
                languageIndex: 'bg-BG',
                languagePlatf: '1'
            },
            {
                languageCode: 'ca',
                languageName: 'Catalan',
                languageIndex: 'ca-ES',
                languagePlatf: '2'
            },
            {
                languageCode: 'zh-CN',
                languageName: 'Chinese',
                languageIndex: 'zh-CN',
                languagePlatf: '2'
            },
            // {
            //   languageCode: 'zh-CN',
            //   languageName: 'Chinese (China)',
            //   languageIndex: 'zh-CN',
            //   languagePlatf: '2'
            // },
            // {
            //   languageCode: 'zh-TW',
            //   languageName: 'Chinese (Taiwan)',
            //   languageIndex: 'zh-TW',
            //   languagePlatf: '0'
            // },
            // {
            //   languageCode: 'zh-HK',
            //   languageName: 'Chinese (Hong Kong)',
            //   languageIndex: 'zh-HK',
            //   languagePlatf: '0'
            // },
            {
                languageCode: 'hr',
                languageName: 'Croatian',
                languageIndex: 'hr-HR',
                languagePlatf: '2'
            },
            {
                languageCode: 'cs',
                languageName: 'Czech',
                languageIndex: 'cs-CZ',
                languagePlatf: '2'
            },
            {
                languageCode: 'da',
                languageName: 'Danish',
                languageIndex: 'da-DK',
                languagePlatf: '2'
            },
            {
                languageCode: 'nl',
                languageName: 'Dutch',
                languageIndex: 'nl-NL',
                languagePlatf: '2'
            },
            // {
            //   languageCode: 'nl',
            //   languageName: 'Dutch (Netherlands)',
            //   languageIndex: 'nl-NL',
            //   languagePlatf: '2'
            // },
            // {
            //   languageCode: 'nl',
            //   languageName: 'Dutch (Belgium)',
            //   languageIndex: 'nl-BE',
            //   languagePlatf: '0'
            // },
            {
                languageCode: 'en',
                languageName: 'English',
                languageIndex: 'en-GB',
                languagePlatf: '2'
            },
            // {
            //   languageCode: 'en',
            //   languageName: 'English (Great Britain)',
            //   languageIndex: 'en-GB',
            //   languagePlatf: '2'
            // },
            // {
            //   languageCode: 'en',
            //   languageName: 'English (United States)',
            //   languageIndex: 'en-US',
            //   languagePlatf: '2'
            // },
            // {
            //   languageCode: 'en',
            //   languageName: 'English (Southern Africa)',
            //   languageIndex: 'en-ZA',
            //   languagePlatf: '2'
            // },
            // {
            //   languageCode: 'en',
            //   languageName: 'English (Australia)',
            //   languageIndex: 'en-AU',
            //   languagePlatf: '2'
            // },
            // {
            //   languageCode: 'en',
            //   languageName: 'English (Canada)',
            //   languageIndex: 'en-CA',
            //   languagePlatf: '2'
            // },
            // {
            //   languageCode: 'en',
            //   languageName: 'English (India)',
            //   languageIndex: 'en-IN',
            //   languagePlatf: '2'
            // },
            // {
            //   languageCode: 'en',
            //   languageName: 'English (Phillippines)',
            //   languageIndex: 'en-PH',
            //   languagePlatf: '2'
            // },
            // {
            //   languageCode: 'en',
            //   languageName: 'English (New Zealand)',
            //   languageIndex: 'en-NZ',
            //   languagePlatf: '2'
            // },
            // {
            //   languageCode: 'en',
            //   languageName: 'English (Ireland)',
            //   languageIndex: 'en-IE',
            //   languagePlatf: '2'
            // },
            // {
            //   languageCode: 'es',
            //   languageName: 'Español (Mexico)',
            //   languageIndex: 'es-MX',
            //   languagePlatf: '2'
            // },
            // {
            //   languageCode: 'es',
            //   languageName: 'Español (Chile)',
            //   languageIndex: 'es-CL',
            //   languagePlatf: '2'
            // },
            // {
            //   languageCode: 'es',
            //   languageName: 'Español (Colombia)',
            //   languageIndex: 'es-CO',
            //   languagePlatf: '2'
            // },
            {
                languageCode: 'es',
                languageName: 'Español',
                languageIndex: 'es-US',
                languagePlatf: '2'
            },
            // {
            //   languageCode: 'es',
            //   languageName: 'Español (Bolivia)',
            //   languageIndex: 'es-BO',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'es',
            //   languageName: 'Español (Costa-Rica)',
            //   languageIndex: 'es-CR',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'es',
            //   languageName: 'Español (Argentina)',
            //   languageIndex: 'es-Ar',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'es',
            //   languageName: 'Español (Ecuador)',
            //   languageIndex: 'es-EC',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'es',
            //   languageName: 'Español (El Salvador)',
            //   languageIndex: 'es-SV',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'es',
            //   languageName: 'Español (Guatemala)',
            //   languageIndex: 'es-GT',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'es',
            //   languageName: 'Español (Honduras)',
            //   languageIndex: 'es-HN',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'es',
            //   languageName: 'Español (Nicaragua)',
            //   languageIndex: 'es-NI',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'es',
            //   languageName: 'Español (Panama)',
            //   languageIndex: 'es-PA',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'es',
            //   languageName: 'Español (Paraguay)',
            //   languageIndex: 'es-PY',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'es',
            //   languageName: 'Español (Peru)',
            //   languageIndex: 'es-PE',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'es',
            //   languageName: 'Español (Puerto Rico)',
            //   languageIndex: 'es-PR',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'es',
            //   languageName: 'Español (Dominican Republic)',
            //   languageIndex: 'es-DO',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'es',
            //   languageName: 'Español (Uruguay)',
            //   languageIndex: 'es-UY',
            //   languagePlatf: '1'
            // },
            // {
            //   languageCode: 'es',
            //   languageName: 'Español (Venezuela)',
            //   languageIndex: 'es-VE',
            //   languagePlatf: '1'
            // },
            {
                languageCode: 'fi',
                languageName: 'Finish',
                languageIndex: 'fi-FI',
                languagePlatf: '2'
            },
            {
                languageCode: 'fr',
                languageName: 'French',
                languageIndex: 'fr-FR',
                languagePlatf: '2'
            },
            // {
            //   languageCode: 'fr',
            //   languageName: 'French (Switzerland)',
            //   languageIndex: 'fr-CH',
            //   languagePlatf: '0'
            // },
            // {
            //   languageCode: 'fr',
            //   languageName: 'French (Canada)',
            //   languageIndex: 'fr-CA',
            //   languagePlatf: '0'
            // },
            // {
            //   languageCode: 'fr',
            //   languageName: 'French (Belgium)',
            //   languageIndex: 'fr-BE',
            //   languagePlatf: '0'
            // },
            {
                languageCode: 'de',
                languageName: 'German',
                languageIndex: 'de-DE',
                languagePlatf: '2'
            },
            // {
            //   languageCode: 'de',
            //   languageName: 'German (Austria)',
            //   languageIndex: 'de-AT',
            //   languagePlatf: '0'
            // },
            // {
            //   languageCode: 'de',
            //   languageName: 'German (Austria)',
            //   languageIndex: 'de-AT',
            //   languagePlatf: '0'
            // },
            // {
            //   languageCode: 'de',
            //   languageName: 'German (Switzerland)',
            //   languageIndex: 'de-CH',
            //   languagePlatf: '0'
            // },
            // {
            //   languageCode: 'de',
            //   languageName: 'German',
            //   languageIndex: 'de-DE',
            //   languagePlatf: '2'
            // },
            {
                languageCode: 'hi',
                languageName: 'Hindi',
                languageIndex: 'hi-IN',
                languagePlatf: '1'
            },
            {
                languageCode: 'hu',
                languageName: 'Hungarian',
                languageIndex: 'hu-HU',
                languagePlatf: '2'
            },
            {
                languageCode: 'is',
                languageName: 'Icelandic',
                languageIndex: 'is-IS',
                languagePlatf: '1'
            },
            {
                languageCode: 'id',
                languageName: 'Indonesian',
                languageIndex: 'id-ID',
                languagePlatf: '2'
            },
            {
                languageCode: 'it',
                languageName: 'Italian',
                languageIndex: 'it-IT',
                languagePlatf: '2'
            },
            // {
            //   languageCode: 'it',
            //   languageName: 'Italian (Switzerland)',
            //   languageIndex: 'it-CH',
            //   languagePlatf: '0'
            // },
            {
                languageCode: 'ja',
                languageName: 'Japanese',
                languageIndex: 'ja-JP',
                languagePlatf: '2'
            },
            {
                languageCode: 'ko',
                languageName: 'Korean',
                languageIndex: 'ko-KR',
                languagePlatf: '2'
            },
            // {
            //   languageCode: 'lv',
            //   languageName: 'Latvian',
            //   languageIndex: 'ar-SA',
            //   languagePlatf: '2'
            // },
            {
                languageCode: 'lt',
                languageName: 'Lithuanian',
                languageIndex: 'lt-LT',
                languagePlatf: '1'
            },
            {
                languageCode: 'nb',
                languageName: 'Norwegian',
                languageIndex: 'nb-NO',
                languagePlatf: '2'
            },
            {
                languageCode: 'fa',
                languageName: 'Persian',
                languageIndex: 'fa-IR',
                languagePlatf: '1'
            },
            {
                languageCode: 'pl',
                languageName: 'Polish',
                languageIndex: 'pl-PL',
                languagePlatf: '2'
            },
            {
                languageCode: 'pt',
                languageName: 'Portuguese',
                languageIndex: 'pt-PT',
                languagePlatf: '2'
            },
            // {
            //   languageCode: 'pt',
            //   languageName: 'Portuguese (Brasil)',
            //   languageIndex: 'pt-BR',
            //   languagePlatf: '2'
            // },
            {
                languageCode: 'ro',
                languageName: 'Romanian',
                languageIndex: 'ro-RO',
                languagePlatf: '2'
            },
            {
                languageCode: 'ru',
                languageName: 'Russian',
                languageIndex: 'ru-RU',
                languagePlatf: '2'
            },
            {
                languageCode: 'sr',
                languageName: 'Serbian',
                languageIndex: 'sr-RS',
                languagePlatf: '2'
            },
            {
                languageCode: 'sk',
                languageName: 'Slovak',
                languageIndex: 'sk-SK',
                languagePlatf: '2'
            },
            {
                languageCode: 'sl',
                languageName: 'Slovenian',
                languageIndex: 'sl-SI',
                languagePlatf: '1'
            },
            {
                languageCode: 'sv',
                languageName: 'Swedish',
                languageIndex: 'sv-SE',
                languagePlatf: '2'
            },
            {
                languageCode: 'th',
                languageName: 'Thai',
                languageIndex: 'th-TH',
                languagePlatf: '2'
            },
            {
                languageCode: 'tr',
                languageName: 'Turkish',
                languageIndex: 'tr-TR',
                languagePlatf: '2'
            },
            {
                languageCode: 'uk',
                languageName: 'Ukrainian',
                languageIndex: 'uk-UA',
                languagePlatf: '2'
            },
            // {
            //   languageCode: 'cy',
            //   languageName: 'Welsh',
            //   languageIndex: 'ar-SA',
            //   languagePlatf: '2'
            // },
            {
                languageCode: 'zu',
                languageName: 'Zulu',
                languageIndex: 'zu-ZA',
                languagePlatf: '1'
            },
            {
                languageCode: 'vi',
                languageName: 'Vietnamese',
                languageIndex: 'vi-VN',
                languagePlatf: '2'
            },
            {
                languageCode: 'ca',
                languageName: 'Catalan',
                languageIndex: 'ca-ES',
                languagePlatf: '2'
            },
            {
                languageCode: 'ms',
                languageName: 'Malay',
                languageIndex: 'ms-MY',
                languagePlatf: '2'
            },
            {
                languageCode: 'el',
                languageName: 'Greek',
                languageIndex: 'el-GR',
                languagePlatf: '2'
            },
            {
                languageCode: 'he',
                languageName: 'Hebrew',
                languageIndex: 'he-IL',
                languagePlatf: '2'
            },
            {
                languageCode: 'af',
                languageName: 'Afrikaans',
                languageIndex: 'af-ZA',
                languagePlatf: '1'
            },
            {
                languageCode: 'gl',
                languageName: 'Galician',
                languageIndex: 'gl-ES',
                languagePlatf: '1'
            },
            {
                languageCode: 'gl',
                languageName: 'Galician',
                languageIndex: 'gl-ES',
                languagePlatf: '1'
            }
        ];
        this.returnlang = [];
        for (var i = 0; i < this.languages.length; i++) {
            if (this.languages[i].languagePlatf == plat || this.languages[i].languagePlatf == 2) {
                this.returnlang.push(this.languages[i]);
            }
        }
        return this.returnlang;
    };
    return TranslationProvider;
}());
TranslationProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
], TranslationProvider);

//# sourceMappingURL=translation.js.map

/***/ }),

/***/ 237:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SellangPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SellangPage = (function () {
    function SellangPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.languages = [];
        this.languages = this.navParams.get('languages');
        this.sellang = this.navParams.get('lang');
    }
    SellangPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad SellangPage');
    };
    SellangPage.prototype.close = function (lang) {
        this.viewCtrl.dismiss(lang);
    };
    return SellangPage;
}());
SellangPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-sellang',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/Translate/src/pages/sellang/sellang.html"*/'<ion-list *ngIf="languages">\n  <ion-item *ngFor="let langs of languages" (click)="close(langs)">\n      {{langs.languageName}} <ion-icon *ngIf="sellang == langs" name="checkmark" color="secondary" item-end></ion-icon>\n  </ion-item>\n</ion-list>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/Translate/src/pages/sellang/sellang.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ViewController */]])
], SellangPage);

//# sourceMappingURL=sellang.js.map

/***/ }),

/***/ 262:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TextmodalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TextmodalPage = (function () {
    function TextmodalPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.word = navParams.get('word');
        this.fromorto = navParams.get('fromorto');
        this.title = navParams.get('title');
        if (this.fromorto == 'from') {
            this.langtxt = this.htmlEnt(this.word.lfromtxt);
            this.langlang = this.word.lfrom.languageName;
        }
        else if (this.fromorto == 'to') {
            this.langtxt = this.htmlEnt(this.word.ltotxt);
            this.langlang = this.word.lto.languageName;
        }
    }
    TextmodalPage.prototype.htmlEnt = function (value) {
        var element = document.getElementById('htmlentitydecodediv');
        if (value && typeof value === 'string') {
            // strip script/html tags
            value = value.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
            value = value.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
            element.innerHTML = value;
            value = element.textContent;
            element.textContent = '';
        }
        return value;
    };
    TextmodalPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    TextmodalPage.prototype.save = function () {
        if (this.fromorto == 'from') {
            this.word.lfromtxt = this.langtxt;
        }
        else if (this.fromorto == 'to') {
            this.word.ltotxt = this.langtxt;
        }
        var data = { word: this.word, fromorto: this.fromorto };
        this.viewCtrl.dismiss(data);
    };
    TextmodalPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad TextmodalPage');
    };
    return TextmodalPage;
}());
TextmodalPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-textmodal',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/Translate/src/pages/textmodal/textmodal.html"*/'<ion-header class="header-view" no-border>\n  <ion-navbar [ngStyle]="{\'background-color\': \'#000\'}">\n    <ion-buttons left>\n      <button ion-button color="light" (click)="close()" [ngStyle]="{\'color\': \'#fff\', \'font-size\': \'200%\'}">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title center color="light" [ngStyle]="{\'color\': \'#fff\'}">{{title}}</ion-title>\n    <ion-buttons right>\n      <button ion-button color="light" (click)="save()" [ngStyle]="{\'color\': \'#fff\', \'font-size\': \'200%\'}">\n        <ion-icon name="checkmark"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding class="main-view">\n  <div class="overlay" (click)="dismiss()"></div>\n  <div class="modal_content" no-border>\n      <p>{{langlang}}</p>\n      <ion-textarea type="text" [(ngModel)]="langtxt" no-border></ion-textarea>\n  </div>\n</ion-content>'/*ion-inline-end:"/Volumes/WorkPalace/Apps/Translate/src/pages/textmodal/textmodal.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ViewController */]])
], TextmodalPage);

//# sourceMappingURL=textmodal.js.map

/***/ }),

/***/ 264:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FullscreenPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_text_to_speech__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_database_database__ = __webpack_require__(72);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FullscreenPage = (function () {
    function FullscreenPage(navCtrl, navParams, viewCtrl, tts, databaseProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.tts = tts;
        this.databaseProvider = databaseProvider;
        this.word = navParams.get('word');
        this.fromorto = navParams.get('fromorto');
        if (this.fromorto == 'from') {
            this.langtxt = this.htmlEnt(this.word.lfromtxt);
            this.lang = this.word.lfrom.languageIndex;
        }
        else if (this.fromorto == 'to') {
            this.langtxt = this.htmlEnt(this.word.ltotxt);
            this.lang = this.word.lto.languageIndex;
        }
    }
    FullscreenPage.prototype.htmlEnt = function (value) {
        var element = document.getElementById('htmlentitydecodediv');
        if (value && typeof value === 'string') {
            // strip script/html tags
            value = value.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
            value = value.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
            element.innerHTML = value;
            value = element.textContent;
            element.textContent = '';
        }
        return value;
    };
    FullscreenPage.prototype.listenWords = function (aword, alang) {
        var _this = this;
        this.databaseProvider.getSetts().then(function (res) {
            var speed = parseInt(res[0].voicespeed) / 10;
            _this.tts.speak({ text: _this.htmlEnt(aword), locale: alang, rate: speed })
                .then(function () { })
                .catch(function (reason) { return alert(reason); });
        });
    };
    FullscreenPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    FullscreenPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad FullscreenPage');
    };
    return FullscreenPage;
}());
FullscreenPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-fullscreen',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/Translate/src/pages/fullscreen/fullscreen.html"*/'<ion-header class="header-view" no-border>\n\n  <ion-navbar>\n    <ion-buttons right>\n      <button ion-button (click)="close()" [ngStyle]="{\'color\': \'#fff\', \'font-size\': \'200%\'}">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding class="glob_back_grad">\n  <h1 [ngStyle]="{\'color\': \'#fff\'}">{{htmlEnt(langtxt)}}</h1>\n  <ion-list>\n    <ion-item text-wrap>\n      <ion-icon (click)="listenWords(langtxt, word.lfrom.languageIndex)" name="custom_sound_act" item-end></ion-icon>\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/Translate/src/pages/fullscreen/fullscreen.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_text_to_speech__["a" /* TextToSpeech */], __WEBPACK_IMPORTED_MODULE_3__providers_database_database__["a" /* DatabaseProvider */]])
], FullscreenPage);

//# sourceMappingURL=fullscreen.js.map

/***/ }),

/***/ 265:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_database_database__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser__ = __webpack_require__(266);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SettingsPage = (function () {
    function SettingsPage(navCtrl, navParams, databaseProvider, alertCtrl, iab) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.databaseProvider = databaseProvider;
        this.alertCtrl = alertCtrl;
        this.iab = iab;
        this.gender = 0;
        this.speed = 10;
        this.autoplay = 0;
        this.icloud = 0;
        this.something = 0;
        this.getDB();
    }
    SettingsPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad SettingsPage');
    };
    SettingsPage.prototype.getDB = function () {
        var _this = this;
        this.databaseProvider.getSetts().then(function (res) {
            _this.gender = res[0].gend;
            _this.speed = res[0].voicespeed;
            _this.autoplay = res[0].autoplay;
            // console.log(res[0].gend + ' ' + res[0].voicespeed + ' ' + res[0].autoplay)
        });
    };
    SettingsPage.prototype.emptyDB = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Delete history?',
            message: 'Confirmation will delete all the translation history.',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                        // console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: function () {
                        // console.log('Agree clicked');
                        _this.databaseProvider.delDB();
                    }
                }
            ]
        });
        confirm.present();
    };
    SettingsPage.prototype.updDB = function () {
        // console.log(this.gender + ' ' + this.speed + ' ' + this.autoplay)
        this.databaseProvider.updSetts(this.gender, this.speed, this.autoplay);
    };
    SettingsPage.prototype.openURL = function (url) {
        this.iab.create(url, '_system', 'location=yes');
    };
    return SettingsPage;
}());
SettingsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-settings',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/Translate/src/pages/settings/settings.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>Settings</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-list radio-group (ionChange)="updDB()" [(ngModel)]="gender">\n    <ion-list-header>\n      Gender\n    </ion-list-header>\n    <ion-item>\n      <ion-label><ion-icon name="custom_voice_male" start></ion-icon> Male</ion-label>\n      <ion-radio checked value="male"></ion-radio>\n    </ion-item>\n    <ion-item>\n      <ion-label><ion-icon name="custom_voice_female" start></ion-icon> Female</ion-label>\n      <ion-radio value="female"></ion-radio>\n    </ion-item>\n  </ion-list>\n  <ion-item-divider text-wrap>\n      Please note, that some languages have only female pronunciation\n  </ion-item-divider>\n  <ion-list>\n      <ion-list-header>\n        Voice speed\n      </ion-list-header>\n      <ion-item>\n          <ion-range min="0" max="100" step="10" (ionChange)="updDB()" [(ngModel)]="speed">\n            <ion-icon range-left small name="custom_low_speed"></ion-icon>\n            <ion-icon range-right name="custom_high_speed"></ion-icon>\n          </ion-range>\n      </ion-item>\n    </ion-list>\n    <ion-list>\n      <ion-list-header>\n        SETTINGS\n      </ion-list-header>\n      <ion-item>\n        <ion-label> Autoplay</ion-label>\n        <ion-toggle (ionChange)="updDB()" [(ngModel)]="autoplay"></ion-toggle>\n      </ion-item>\n      <ion-item>\n        <ion-label> iCloud</ion-label>\n        <ion-toggle checked="true"></ion-toggle>\n      </ion-item>\n      <ion-item>\n        <ion-label> Something else</ion-label>\n        <ion-toggle checked="true"></ion-toggle>\n      </ion-item>\n    </ion-list>\n    <ion-list>\n      <ion-list-header>\n        ADDITIONALLY\n      </ion-list-header>\n      <button ion-item (click)="openURL(\'http://www.profify.me\')">\n          <ion-icon name="custom_help" item-start></ion-icon>\n          Help\n      </button>\n      <button ion-item (click)="openURL(\'http://www.profify.me\')">\n          <ion-icon name="custom_support" item-start></ion-icon>\n          Support\n      </button> \n      <button ion-item (click)="openURL(\'http://www.profify.me\')">\n          <ion-icon name="custom_rate" item-start></ion-icon>\n          Rate us\n      </button> \n    </ion-list>\n    <button ion-button color="danger" clear full (click)="emptyDB()">Clear translation history</button>\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/Translate/src/pages/settings/settings.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser__["a" /* InAppBrowser */]])
], SettingsPage);

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContactPage = (function () {
    function ContactPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    return ContactPage;
}());
ContactPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-contact',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/Translate/src/pages/contact/contact.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Contact\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-list-header>Follow us on Twitter</ion-list-header>\n    <ion-item>\n      <ion-icon name="ionic" item-left></ion-icon>\n      @ionicframework\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/Translate/src/pages/contact/contact.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]])
], ContactPage);

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_speech_recognition__ = __webpack_require__(125);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = (function () {
    function HomePage(navCtrl, speechRecognition, plt, cd) {
        this.navCtrl = navCtrl;
        this.speechRecognition = speechRecognition;
        this.plt = plt;
        this.cd = cd;
        this.isRecording = false;
    }
    HomePage.prototype.isIos = function () {
        return this.plt.is('ios');
    };
    HomePage.prototype.stopListening = function () {
        var _this = this;
        this.speechRecognition.stopListening().then(function () {
            _this.isRecording = false;
        });
    };
    HomePage.prototype.getPermission = function () {
        var _this = this;
        this.speechRecognition.hasPermission()
            .then(function (hasPermission) {
            if (!hasPermission) {
                _this.speechRecognition.requestPermission();
            }
        });
    };
    HomePage.prototype.startListening = function () {
        var _this = this;
        var options = {
            language: 'en-US'
        };
        this.speechRecognition.startListening(options).subscribe(function (matches) {
            _this.matches = matches;
            _this.cd.detectChanges();
        });
        this.isRecording = true;
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/Translate/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar [color]="isRecording ? \'danger\' : \'primary\'">\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  \n  <button ion-button full (click)="getPermission()">Get Permission</button>\n  <button ion-button full (click)="startListening()">Start Listening</button>\n  <button ion-button full (click)="stopListening()" *ngIf="isIos()">Stop Listening</button>\n\n  <ion-card>\n    <ion-card-header>This is what I understood...</ion-card-header>\n    <ion-card-content>\n      <ion-list>\n        <ion-item *ngFor="let match of matches">\n          {{ match }}\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/Translate/src/pages/home/home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_speech_recognition__["a" /* SpeechRecognition */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 270:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(271);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_module__ = __webpack_require__(275);



Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_20" /* enableProdMode */])();
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_about_about__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__ = __webpack_require__(584);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_sellang_sellang__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_textmodal_textmodal__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_fullscreen_fullscreen__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_settings_settings__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_status_bar__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_speech_recognition__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_translation_translation__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__angular_http__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__angular_platform_browser_animations__ = __webpack_require__(585);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_sqlite__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_storage__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__providers_database_database__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_text_to_speech__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_clipboard__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_network__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_in_app_browser__ = __webpack_require__(266);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

























var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_4__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_sellang_sellang__["a" /* SellangPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_textmodal_textmodal__["a" /* TextmodalPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_fullscreen_fullscreen__["a" /* FullscreenPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_settings_settings__["a" /* SettingsPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */]),
            __WEBPACK_IMPORTED_MODULE_16__angular_http__["b" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_17__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
            __WEBPACK_IMPORTED_MODULE_19__ionic_storage__["a" /* IonicStorageModule */].forRoot()
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_4__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_sellang_sellang__["a" /* SellangPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_textmodal_textmodal__["a" /* TextmodalPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_fullscreen_fullscreen__["a" /* FullscreenPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_settings_settings__["a" /* SettingsPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_12__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_14__ionic_native_speech_recognition__["a" /* SpeechRecognition */],
            { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_15__providers_translation_translation__["a" /* TranslationProvider */],
            __WEBPACK_IMPORTED_MODULE_18__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_20__providers_database_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_21__ionic_native_text_to_speech__["a" /* TextToSpeech */],
            __WEBPACK_IMPORTED_MODULE_22__ionic_native_clipboard__["a" /* Clipboard */],
            __WEBPACK_IMPORTED_MODULE_23__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_24__ionic_native_in_app_browser__["a" /* InAppBrowser */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 312:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_about_about__ = __webpack_require__(124);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { TabsPage } from '../pages/tabs/tabs';

var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        // rootPage:any = TabsPage;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_about_about__["a" /* AboutPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    return MyApp;
}());
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/Translate/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/Translate/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 584:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__about_about__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_contact__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_animations__ = __webpack_require__(128);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsPage = (function () {
    function TabsPage() {
        this.flipState = 'flipped';
        this.flipState2 = 'flipped2';
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_1__about_about__["a" /* AboutPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__contact_contact__["a" /* ContactPage */];
    }
    TabsPage.prototype.ngAfterViewInit = function () {
        // console.log(this.btmTbs);
    };
    TabsPage.prototype.flipStopped = function (ev) {
        // console.log(ev);
    };
    TabsPage.prototype.flipStarted = function (ev) {
        // console.log(ev);
    };
    TabsPage.prototype.toggleFlip = function () {
        this.flipState = (this.flipState == 'notFlipped') ? 'flipped' : 'notFlipped';
    };
    TabsPage.prototype.toggleFlip2 = function () {
        this.flipState2 = (this.flipState2 == 'notFlipped2') ? 'flipped2' : 'notFlipped2';
    };
    return TabsPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])('btmTbs'),
    __metadata("design:type", Object)
], TabsPage.prototype, "btmTbs", void 0);
TabsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/Translate/src/pages/tabs/tabs.html"*/'<ion-tabs>\n  <ion-tab [root]="tab1Root" tabIcon="custom_settings"></ion-tab>\n  <ion-tab [root]="tab2Root" tabIcon="custom_mic"></ion-tab>\n  <ion-tab [root]="tab3Root" tabIcon="custom_translate"></ion-tab>\n  <p id="x" [@flip]=\'flipState\' (@flip.start)=\'flipStarted($event)\' (@flip.done)=\'toggleFlip()\'></p>\n  <p id="y" [@flip2]=\'flipState2\' (@flip2.done)=\'toggleFlip2()\'></p>\n</ion-tabs>'/*ion-inline-end:"/Volumes/WorkPalace/Apps/Translate/src/pages/tabs/tabs.html"*/,
        styles: ["\n  p#x {\n    position: absolute;\n    z-index: 11;\n    bottom:10%;\n    left: 50%;\n    margin: 0 0 -32px -31px;\n    width:62px;\n    text-align:center;\n    content: url(../../assets/icon/mic_ring_in.svg);\n    opacity: 0.9;\n  }\n  p#y {\n    position: absolute;\n    z-index: 11;\n    bottom:10%;\n    left: 50%;\n    margin: 0 0 -39px -38px;\n    width:76px;\n    text-align:center;\n    content: url(../../assets/icon/mic_ring_out.svg);\n    opacity: 0.9;\n  }\n  "],
        animations: [
            Object(__WEBPACK_IMPORTED_MODULE_4__angular_animations__["h" /* trigger */])('flip', [
                Object(__WEBPACK_IMPORTED_MODULE_4__angular_animations__["e" /* state */])('flipped', Object(__WEBPACK_IMPORTED_MODULE_4__angular_animations__["f" /* style */])({
                    transform: 'rotate(360deg)',
                })),
                Object(__WEBPACK_IMPORTED_MODULE_4__angular_animations__["g" /* transition */])('* => flipped', Object(__WEBPACK_IMPORTED_MODULE_4__angular_animations__["c" /* animate */])('4000ms linear'))
            ]),
            Object(__WEBPACK_IMPORTED_MODULE_4__angular_animations__["h" /* trigger */])('flip2', [
                Object(__WEBPACK_IMPORTED_MODULE_4__angular_animations__["e" /* state */])('flipped2', Object(__WEBPACK_IMPORTED_MODULE_4__angular_animations__["f" /* style */])({
                    transform: 'rotate(-360deg)',
                })),
                Object(__WEBPACK_IMPORTED_MODULE_4__angular_animations__["g" /* transition */])('* => flipped2', Object(__WEBPACK_IMPORTED_MODULE_4__angular_animations__["c" /* animate */])('5000ms linear'))
            ])
        ]
    }),
    __metadata("design:paramtypes", [])
], TabsPage);

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 72:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatabaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_sqlite__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DatabaseProvider = (function () {
    function DatabaseProvider(platform, http, sqlite, storage) {
        var _this = this;
        this.platform = platform;
        this.http = http;
        this.sqlite = sqlite;
        this.storage = storage;
        this.databaseReady = new __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__["BehaviorSubject"](false);
        this.platform.ready().then(function () {
            _this.sqlite.create({
                name: 'data.db',
                location: 'default'
            })
                .then(function (db) {
                _this.database = db;
                _this.storage.get('database_filled').then(function (val) {
                    if (val) {
                        _this.databaseReady.next(true);
                    }
                    else {
                        db.executeSql('CREATE TABLE IF NOT EXISTS words(id integer primary key, lfrom text, lto text, lfromtxt text, ltotxt text, ttime text, tdel integer)', {})
                            .then(function () { return _this.storage.set('database_filled', true); })
                            .catch(function (e) { return console.log(e); });
                        db.executeSql('CREATE TABLE IF NOT EXISTS setts(id integer primary key, gend text, voicespeed integer, autoplay integer, preflangfrom text, preflangto text, lastlangfrom text, lastlangto text, registered integer, modified integer)', {})
                            .then(function () {
                            _this.storage.set('database_filled', true);
                            var thetime = new Date().getTime();
                            db.executeSql('INSERT INTO setts (id, gend, voicespeed, autoplay, preflangfrom, preflangto, lastlangfrom, lastlangto, registered, modified) VALUES (?,?,?,?,?,?,?,?,?,?)', [1, 'male', 100, 0, 0, 0, 0, 0, thetime, thetime]).then(function (res) { }, function (error) { });
                        })
                            .catch(function (e) { return console.log(e); });
                    }
                });
            })
                .catch(function (e) { return console.log(e); });
        });
    }
    DatabaseProvider.prototype.stringifi = function (data) {
        return JSON.stringify(data);
    };
    DatabaseProvider.prototype.getDatabaseState = function () {
        return this.databaseReady.asObservable();
    };
    DatabaseProvider.prototype.addDb = function (lfrom, lto, lfromtxt, ltotxt) {
        var _this = this;
        var letfrom = this.stringifi(lfrom);
        var letto = this.stringifi(lto);
        var data = [letfrom, letto, lfromtxt, ltotxt, new Date().getTime(), 0];
        this.database.executeSql("INSERT INTO words (lfrom, lto, lfromtxt, ltotxt, ttime, tdel) VALUES (?,?,?,?,?,?)", data).then(function (res) {
            // console.log("INSERTED: " + this.stringifi(res));
        }, function (error) {
            console.log("ERROR: " + _this.stringifi(error.err));
        });
    };
    DatabaseProvider.prototype.getDB = function () {
        var _this = this;
        return this.database.executeSql("SELECT * FROM words WHERE tdel != ? ORDER BY id DESC", [1]).then(function (res) {
            var words = [];
            if (res.rows.length > 0) {
                for (var i = 0; i < res.rows.length; i++) {
                    var reslfrom = JSON.parse(res.rows.item(i).lfrom);
                    var reslto = JSON.parse(res.rows.item(i).lto);
                    words.push({ id: res.rows.item(i).id, lfrom: reslfrom, lto: reslto, lfromtxt: res.rows.item(i).lfromtxt, ltotxt: res.rows.item(i).ltotxt, ttime: res.rows.item(i).ttime });
                }
            }
            return words;
        }, function (error) {
            console.log("ERROR: " + _this.stringifi(error));
        });
    };
    DatabaseProvider.prototype.getDDB = function (id) {
        var _this = this;
        var data = [id, 1];
        return this.database.executeSql("SELECT * FROM words WHERE id=? && tdel != ?", data).then(function (res) {
            var word = [];
            if (res.rows.length > 0) {
                for (var i = 0; i < res.rows.length; i++) {
                    if (res.rows.item(i).tdel != '1') {
                        var reslfrom = JSON.parse(res.rows.item(i).lfrom);
                        var reslto = JSON.parse(res.rows.item(i).lto);
                        word.push({ id: res.rows.item(i).id, lfrom: reslfrom, lto: reslto, ltotxt: res.rows.item(i).ltotxt, ttime: res.rows.item(i).ttime });
                    }
                }
            }
            return word;
        }, function (error) {
            console.log("ERROR: " + _this.stringifi(error));
        });
    };
    DatabaseProvider.prototype.updDDB = function (id, lfrom, lto, lfromtxt, ltotxt, ttime, tdel) {
        var _this = this;
        var letfrom = this.stringifi(lfrom);
        var letto = this.stringifi(lto);
        var data = [letfrom, letto, lfromtxt, ltotxt, ttime, tdel, id];
        this.database.executeSql("UPDATE words SET lfrom=?, lto=?, lfromtxt=?, ltotxt=?, ttime=?, tdel=? WHERE id =?", data).then(function (res) {
            // console.log("OK: " + this.stringifi(res));
        }, function (error) {
            console.log("ERROR: " + _this.stringifi(error));
        });
    };
    DatabaseProvider.prototype.delDB = function () {
        var _this = this;
        this.database.executeSql("UPDATE words SET tdel=?", [1]).then(function (res) {
            // console.log("OK: " + this.stringifi(res));
        }, function (error) {
            console.log("ERROR: " + _this.stringifi(error));
        });
    };
    DatabaseProvider.prototype.getSetts = function () {
        var _this = this;
        return this.database.executeSql("SELECT * FROM setts WHERE id = ?", [1]).then(function (res) {
            var setts = [];
            if (res.rows.length > 0) {
                for (var i = 0; i < res.rows.length; i++) {
                    setts.push(res.rows.item(i));
                }
            }
            return setts;
        }, function (error) {
            console.log("ERROR: " + _this.stringifi(error));
        });
    };
    DatabaseProvider.prototype.updSetts = function (gend, voicespeed, autoplay) {
        var _this = this;
        var data = [gend, voicespeed, autoplay, 0, 0, 0, 0, new Date().getTime(), 1];
        this.database.executeSql("UPDATE setts SET gend=?, voicespeed=?, autoplay=?, preflangfrom=?, preflangto=?, lastlangfrom=?, lastlangto=?, modified=? WHERE id = ?", data).then(function (res) {
            // console.log("OK: " + this.stringifi(res));
        }, function (error) {
            console.log("ERROR: " + _this.stringifi(error));
        });
    };
    return DatabaseProvider;
}());
DatabaseProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_0__ionic_native_sqlite__["a" /* SQLite */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]])
], DatabaseProvider);

//# sourceMappingURL=database.js.map

/***/ })

},[270]);
//# sourceMappingURL=main.js.map